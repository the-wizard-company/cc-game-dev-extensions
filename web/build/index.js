'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

/**
 *  Typescript implementation of CSInterface - v7.0.0
 *
 *  Custom CSInterface.js implementation in Typescript.
 *  The implementation covers version 7.x from original repository, with all functionalities.
 *  All documentation comments were also based on original.
 *
 *
 *  For more information about Creative Cloud extensions development
 *  please refer to original Github page: https://github.com/Adobe-CEP/CEP-Resources
 */
/**
 * @class CSInterface
 * This is the entry point to the CEP extensibility infrastructure.
 * Instantiate this object and use it to:
 * <ul>
 * <li>Access information about the host application in which an extension is running</li>
 * <li>Launch an extension</li>
 * <li>Register interest in event notifications, and dispatch events</li>
 * </ul>
 *
 */
class CSInterface {
    constructor() {
        /**
         * User can add this event listener to handle native application theme color changes.
         * Callback function gives extensions ability to fine-tune their theme color after the
         * global theme color has been changed.
         * The callback function should be like below:
         *
         * @example
         * // event is a CSEvent object, but user can ignore it.
         * function OnAppThemeColorChanged(event)
         * {
         *    // Should get a latest HostEnvironment object from application.
         *    var skinInfo = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
         *    // Gets the style information such as color info from the skinInfo,
         *    // and redraw all UI controls of your extension according to the style info.
         * }
         */
        this.THEME_COLOR_CHANGED_EVENT = "com.adobe.csxs.events.ThemeColorChanged";
        /** The host environment data object. */
        this.hostEnvironment = JSON.parse(window.__adobe_cep__.getHostEnvironment());
    }
    /** Retrieves information about the host environment in which the
     *  extension is currently running.
     *
     *   @return A \c #HostEnvironment object.
     */
    getHostEnvironment() {
        this.hostEnvironment = JSON.parse(window.__adobe_cep__.getHostEnvironment());
        return this.hostEnvironment;
    }
    ;
    /** Closes this extension. */
    closeExtension() {
        window.__adobe_cep__.closeExtension();
    }
    ;
    /**
     * Retrieves a path for which a constant is defined in the system.
     *
     * @param pathType The path-type constant defined in \c #SystemPath ,
     *
     * @return The platform-specific system path string.
     */
    getSystemPath(pathType) {
        var path = decodeURI(window.__adobe_cep__.getSystemPath(pathType));
        var OSVersion = this.getOSInformation();
        if (OSVersion.indexOf("Windows") >= 0) {
            path = path.replace("file:///", "");
        }
        else if (OSVersion.indexOf("Mac") >= 0) {
            path = path.replace("file://", "");
        }
        return path;
    }
    ;
    /**
     * Evaluates a JavaScript script, which can use the JavaScript DOM
     * of the host application.
     *
     * @param script    The JavaScript script.
     * @param callback  Optional. A callback function that receives the result of execution.
     *          If execution fails, the callback function receives the error message \c EvalScript_ErrMessage.
     */
    evalScript(script, callback) {
        if (callback === null || callback === undefined) {
            callback = (result) => { };
        }
        window.__adobe_cep__.evalScript(script, callback);
    }
    ;
    /**
     * Retrieves the unique identifier of the application.
     * in which the extension is currently running.
     *
     * @return The unique ID string.
     */
    getApplicationID() {
        var appId = this.hostEnvironment.appId;
        return appId;
    }
    ;
    /**
     * Retrieves host capability information for the application
     * in which the extension is currently running.
     *
     * @return A \c #HostCapabilities object.
     */
    getHostCapabilities() {
        var hostCapabilities = JSON.parse(window.__adobe_cep__.getHostCapabilities());
        return hostCapabilities;
    }
    ;
    /**
     * Triggers a CEP event programmatically. Yoy can use it to dispatch
     * an event of a predefined type, or of a type you have defined.
     *
     * @param event A \c CSEvent object.
     */
    dispatchEvent(event) {
        if (typeof event.data == "object") {
            event.data = JSON.stringify(event.data);
        }
        window.__adobe_cep__.dispatchEvent(event);
    }
    ;
    /**
     * Registers an interest in a CEP event of a particular type, and
     * assigns an event handler.
     * The event infrastructure notifies your extension when events of this type occur,
     * passing the event object to the registered handler function.
     *
     * @param type     The name of the event type of interest.
     * @param listener The JavaScript handler function or method.
     * @param obj      Optional, the object containing the handler method, if any.
     *         Default is null.
     */
    addEventListener(type, listener, obj) {
        window.__adobe_cep__.addEventListener(type, listener, obj);
    }
    ;
    /**
     * Removes a registered event listener.
     *
     * @param type      The name of the event type of interest.
     * @param listener  The JavaScript handler function or method that was registered.
     * @param obj       Optional, the object containing the handler method, if any.
     *          Default is null.
     */
    removeEventListener(type, listener, obj) {
        window.__adobe_cep__.removeEventListener(type, listener, obj);
    }
    ;
    /**
     * Loads and launches another extension, or activates the extension if it is already loaded.
     *
     * @param extensionId       The extension's unique identifier.
     * @param startupParams     Not currently used, pass "".
     *
     * @example
     * To launch the extension "help" with ID "HLP" from this extension, call:
     * <code>requestOpenExtension("HLP", ""); </code>
     *
     */
    requestOpenExtension(extensionId, params) {
        window.__adobe_cep__.requestOpenExtension(extensionId, params);
    }
    ;
    /**
     * Retrieves the list of extensions currently loaded in the current host application.
     * The extension list is initialized once, and remains the same during the lifetime
     * of the CEP session.
     *
     * @param extensionIds  Optional, an array of unique identifiers for extensions of interest.
     *          If omitted, retrieves data for all extensions.
     *
     * @return Zero or more \c #Extension objects.
     */
    getExtensions(extensionIds) {
        var extensionIdsStr = JSON.stringify(extensionIds);
        var extensionsStr = window.__adobe_cep__.getExtensions(extensionIdsStr);
        var extensions = JSON.parse(extensionsStr);
        return extensions;
    }
    ;
    /**
     * Retrieves network-related preferences.
     *
     * @return A JavaScript object containing network preferences.
     */
    getNetworkPreferences() {
        var result = window.__adobe_cep__.getNetworkPreferences();
        var networkPre = JSON.parse(result);
        return networkPre;
    }
    ;
    /**
     * Initializes the resource bundle for this extension with property values
     * for the current application and locale.
     * To support multiple locales, you must define a property file for each locale,
     * containing keyed display-string values for that locale.
     * See localization documentation for Extension Builder and related products.
     *
     * Keys can be in the
     * form <code>key.value="localized string"</code>, for use in HTML text elements.
     * For example, in this input element, the localized \c key.value string is displayed
     * instead of the empty \c value string:
     *
     * <code><input type="submit" value="" data-locale="key"/></code>
     *
     * @return An object containing the resource bundle information.
     */
    initResourceBundle() {
        var resourceBundle = JSON.parse(window.__adobe_cep__.initResourceBundle());
        var resElms = document.querySelectorAll('[data-locale]');
        for (var n = 0; n < resElms.length; n++) {
            var resEl = resElms[n];
            // Get the resource key from the element.
            var resKey = resEl.getAttribute('data-locale');
            if (resKey) {
                // Get all the resources that start with the key.
                for (var key in resourceBundle) {
                    if (key.indexOf(resKey) === 0) {
                        var resValue = resourceBundle[key];
                        if (key.length == resKey.length) {
                            resEl.innerHTML = resValue;
                        }
                        else if ('.' == key.charAt(resKey.length)) {
                            var attrKey = key.substring(resKey.length + 1);
                            resEl[attrKey] = resValue;
                        }
                    }
                }
            }
        }
        return resourceBundle;
    }
    ;
    /**
     * Writes installation information to a file.
     *
     * @return The file path.
     */
    dumpInstallationInfo() {
        return window.__adobe_cep__.dumpInstallationInfo();
    }
    ;
    /**
     * Retrieves version information for the current Operating System,
     * See http://www.useragentstring.com/pages/Chrome/ for Chrome \c navigator.userAgent values.
     *
     * @return A string containing the OS version, or "unknown Operation System".
     * If user customizes the User Agent by setting CEF command parameter "--user-agent", only
     * "Mac OS X" or "Windows" will be returned.
     */
    getOSInformation() {
        var userAgent = navigator.userAgent;
        if ((navigator.platform == "Win32") || (navigator.platform == "Windows")) {
            var winVersion = "Windows";
            var winBit = "";
            if (userAgent.indexOf("Windows") > -1) {
                if (userAgent.indexOf("Windows NT 5.0") > -1) {
                    winVersion = "Windows 2000";
                }
                else if (userAgent.indexOf("Windows NT 5.1") > -1) {
                    winVersion = "Windows XP";
                }
                else if (userAgent.indexOf("Windows NT 5.2") > -1) {
                    winVersion = "Windows Server 2003";
                }
                else if (userAgent.indexOf("Windows NT 6.0") > -1) {
                    winVersion = "Windows Vista";
                }
                else if (userAgent.indexOf("Windows NT 6.1") > -1) {
                    winVersion = "Windows 7";
                }
                else if (userAgent.indexOf("Windows NT 6.2") > -1) {
                    winVersion = "Windows 8";
                }
                else if (userAgent.indexOf("Windows NT 6.3") > -1) {
                    winVersion = "Windows 8.1";
                }
                else if (userAgent.indexOf("Windows NT 10") > -1) {
                    winVersion = "Windows 10";
                }
                if (userAgent.indexOf("WOW64") > -1 || userAgent.indexOf("Win64") > -1) {
                    winBit = " 64-bit";
                }
                else {
                    winBit = " 32-bit";
                }
            }
            return winVersion + winBit;
        }
        else if ((navigator.platform == "MacIntel") || (navigator.platform == "Macintosh")) {
            var result = "Mac OS X";
            if (userAgent.indexOf("Mac OS X") > -1) {
                result = userAgent.substring(userAgent.indexOf("Mac OS X"), userAgent.indexOf(")"));
                result = result.replace(/_/g, ".");
            }
            return result;
        }
        return "Unknown Operation System";
    }
    ;
    /**
     * Opens a page in the default system browser.
     *
     * Since 4.2.0
     *
     * @param url  The URL of the page/file to open, or the email address.
     * Must use HTTP/HTTPS/file/mailto protocol. For example:
     *   "http://www.adobe.com"
     *   "https://github.com"
     *   "file:///C:/log.txt"
     *   "mailto:test@adobe.com"
     *
     * @return One of these error codes:\n
     *      <ul>\n
     *          <li>NO_ERROR - 0</li>\n
     *          <li>ERR_UNKNOWN - 1</li>\n
     *          <li>ERR_INVALID_PARAMS - 2</li>\n
     *          <li>ERR_INVALID_URL - 201</li>\n
     *      </ul>\n
     */
    openURLInDefaultBrowser(url) {
        return cep.util.openURLInDefaultBrowser(url);
    }
    ;
    /**
     * Retrieves extension ID.
     *
     * Since 4.2.0
     *
     * @return extension ID.
     */
    getExtensionID() {
        return window.__adobe_cep__.getExtensionId();
    }
    ;
    /**
     * Retrieves the scale factor of screen.
     * On Windows platform, the value of scale factor might be different from operating system's scale factor,
     * since host application may use its self-defined scale factor.
     *
     * Since 4.2.0
     *
     * @return One of the following float number.
     *      <ul>\n
     *          <li> -1.0 when error occurs </li>\n
     *          <li> 1.0 means normal screen </li>\n
     *          <li> >1.0 means HiDPI screen </li>\n
     *      </ul>\n
     */
    getScaleFactor() {
        return window.__adobe_cep__.getScaleFactor();
    }
    ;
    /**
     * Set a handler to detect any changes of scale factor. This only works on Mac.
     *
     * Since 4.2.0
     *
     * @param handler   The function to be called when scale factor is changed.
     *
     */
    setScaleFactorChangedHandler(handler) {
        window.__adobe_cep__.setScaleFactorChangedHandler(handler);
    }
    ;
    /**
     * Retrieves current API version.
     *
     * Since 4.2.0
     *
     * @return ApiVersion object.
     *
     */
    getCurrentApiVersion() {
        var apiVersion = JSON.parse(window.__adobe_cep__.getCurrentApiVersion());
        return apiVersion;
    }
    ;
    /**
     * Set panel flyout menu by an XML.
     *
     * Since 5.2.0
     *
     * Register a callback function for "com.adobe.csxs.events.flyoutMenuClicked" to get notified when a
     * menu item is clicked.
     * The "data" attribute of event is an object which contains "menuId" and "menuName" attributes.
     *
     * Register callback functions for "com.adobe.csxs.events.flyoutMenuOpened" and "com.adobe.csxs.events.flyoutMenuClosed"
     * respectively to get notified when flyout menu is opened or closed.
     *
     * @param menu     A XML string which describes menu structure.
     * An example menu XML:
     * <Menu>
     *   <MenuItem Id="menuItemId1" Label="TestExample1" Enabled="true" Checked="false"/>
     *   <MenuItem Label="TestExample2">
     *     <MenuItem Label="TestExample2-1" >
     *       <MenuItem Label="TestExample2-1-1" Enabled="false" Checked="true"/>
     *     </MenuItem>
     *     <MenuItem Label="TestExample2-2" Enabled="true" Checked="true"/>
     *   </MenuItem>
     *   <MenuItem Label="---" />
     *   <MenuItem Label="TestExample3" Enabled="false" Checked="false"/>
     * </Menu>
     *
     */
    setPanelFlyoutMenu(menu) {
        if ("string" != typeof menu) {
            return;
        }
        window.__adobe_cep__.invokeSync("setPanelFlyoutMenu", menu);
    }
    ;
    /**
     * Updates a menu item in the extension window's flyout menu, by setting the enabled
     * and selection status.
     *
     * Since 5.2.0
     *
     * @param menuItemLabel    The menu item label.
     * @param enabled        True to enable the item, false to disable it (gray it out).
     * @param checked        True to select the item, false to deselect it.
     *
     * @return false when the host application does not support this functionality (HostCapabilities.EXTENDED_PANEL_MENU is false).
     *         Fails silently if menu label is invalid.
     *
     * @see HostCapabilities.EXTENDED_PANEL_MENU
     */
    updatePanelMenuItem(menuItemLabel, enabled, checked) {
        var ret = false;
        if (this.getHostCapabilities().EXTENDED_PANEL_MENU) {
            var itemStatus = new MenuItemStatus(menuItemLabel, enabled, checked);
            ret = window.__adobe_cep__.invokeSync("updatePanelMenuItem", JSON.stringify(itemStatus));
        }
        return ret;
    }
    ;
    /**
     * Set context menu by XML string.
     *
     * Since 5.2.0
     *
     * There are a number of conventions used to communicate what type of menu item to create and how it should be handled.
     * - an item without menu ID or menu name is disabled and is not shown.
     * - if the item name is "---" (three hyphens) then it is treated as a separator. The menu ID in this case will always be NULL.
     * - Checkable attribute takes precedence over Checked attribute.
     * - a PNG icon. For optimal display results please supply a 16 x 16px icon as larger dimensions will increase the size of the menu item.
     The Chrome extension contextMenus API was taken as a reference.
     https://developer.chrome.com/extensions/contextMenus
     * - the items with icons and checkable items cannot coexist on the same menu level. The former take precedences over the latter.
     *
     * @param menu      A XML string which describes menu structure.
     * @param callback  The callback function which is called when a menu item is clicked. The only parameter is the returned ID of clicked menu item.
     *
     * @description An example menu XML:
     * <Menu>
     *   <MenuItem Id="menuItemId1" Label="TestExample1" Enabled="true" Checkable="true" Checked="false" Icon="./image/small_16X16.png"/>
     *   <MenuItem Id="menuItemId2" Label="TestExample2">
     *     <MenuItem Id="menuItemId2-1" Label="TestExample2-1" >
     *       <MenuItem Id="menuItemId2-1-1" Label="TestExample2-1-1" Enabled="false" Checkable="true" Checked="true"/>
     *     </MenuItem>
     *     <MenuItem Id="menuItemId2-2" Label="TestExample2-2" Enabled="true" Checkable="true" Checked="true"/>
     *   </MenuItem>
     *   <MenuItem Label="---" />
     *   <MenuItem Id="menuItemId3" Label="TestExample3" Enabled="false" Checkable="true" Checked="false"/>
     * </Menu>
     */
    setContextMenu(menu, callback) {
        if ("string" != typeof menu) {
            return;
        }
        window.__adobe_cep__.invokeAsync("setContextMenu", menu, callback);
    }
    ;
    /**
     * Set context menu by JSON string.
     *
     * Since 6.0.0
     *
     * There are a number of conventions used to communicate what type of menu item to create and how it should be handled.
     * - an item without menu ID or menu name is disabled and is not shown.
     * - if the item label is "---" (three hyphens) then it is treated as a separator. The menu ID in this case will always be NULL.
     * - Checkable attribute takes precedence over Checked attribute.
     * - a PNG icon. For optimal display results please supply a 16 x 16px icon as larger dimensions will increase the size of the menu item.
     The Chrome extension contextMenus API was taken as a reference.
     * - the items with icons and checkable items cannot coexist on the same menu level. The former take precedences over the latter.
     https://developer.chrome.com/extensions/contextMenus
     *
     * @param menu      A JSON string which describes menu structure.
     * @param callback  The callback function which is called when a menu item is clicked. The only parameter is the returned ID of clicked menu item.
     *
     * @description An example menu JSON:
     *
     * {
     *      "menu": [
     *          {
     *              "id": "menuItemId1",
     *              "label": "testExample1",
     *              "enabled": true,
     *              "checkable": true,
     *              "checked": false,
     *              "icon": "./image/small_16X16.png"
     *          },
     *          {
     *              "id": "menuItemId2",
     *              "label": "testExample2",
     *              "menu": [
     *                  {
     *                      "id": "menuItemId2-1",
     *                      "label": "testExample2-1",
     *                      "menu": [
     *                          {
     *                              "id": "menuItemId2-1-1",
     *                              "label": "testExample2-1-1",
     *                              "enabled": false,
     *                              "checkable": true,
     *                              "checked": true
     *                          }
     *                      ]
     *                  },
     *                  {
     *                      "id": "menuItemId2-2",
     *                      "label": "testExample2-2",
     *                      "enabled": true,
     *                      "checkable": true,
     *                      "checked": true
     *                  }
     *              ]
     *          },
     *          {
     *              "label": "---"
     *          },
     *          {
     *              "id": "menuItemId3",
     *              "label": "testExample3",
     *              "enabled": false,
     *              "checkable": true,
     *              "checked": false
     *          }
     *      ]
     *  }
     *
     */
    setContextMenuByJSON(menu, callback) {
        if ("string" != typeof menu) {
            return;
        }
        window.__adobe_cep__.invokeAsync("setContextMenuByJSON", menu, callback);
    }
    ;
    /**
     * Updates a context menu item by setting the enabled and selection status.
     *
     * Since 5.2.0
     *
     * @param menuItemID    The menu item ID.
     * @param enabled        True to enable the item, false to disable it (gray it out).
     * @param checked        True to select the item, false to deselect it.
     */
    updateContextMenuItem(menuItemID, enabled, checked) {
        var itemStatus = new ContextMenuItemStatus(menuItemID, enabled, checked);
        ret = window.__adobe_cep__.invokeSync("updateContextMenuItem", JSON.stringify(itemStatus));
    }
    ;
    /**
     * Get the visibility status of an extension window.
     *
     * Since 6.0.0
     *
     * @return true if the extension window is visible; false if the extension window is hidden.
     */
    isWindowVisible() {
        return window.__adobe_cep__.invokeSync("isWindowVisible", "");
    }
    ;
    /**
     * Resize extension's content to the specified dimensions.
     * 1. Works with modal and modeless extensions in all Adobe products.
     * 2. Extension's manifest min/max size constraints apply and take precedence.
     * 3. For panel extensions
     *    3.1 This works in all Adobe products except:
     *        * Premiere Pro
     *        * Prelude
     *        * After Effects
     *    3.2 When the panel is in certain states (especially when being docked),
     *        it will not change to the desired dimensions even when the
     *        specified size satisfies min/max constraints.
     *
     * Since 6.0.0
     *
     * @param width  The new width
     * @param height The new height
     */
    resizeContent(width, height) {
        window.__adobe_cep__.resizeContent(width, height);
    }
    ;
    /**
     * Register the invalid certificate callback for an extension.
     * This callback will be triggered when the extension tries to access the web site that contains the invalid certificate on the main frame.
     * But if the extension does not call this function and tries to access the web site containing the invalid certificate, a default error page will be shown.
     *
     * Since 6.1.0
     *
     * @param callback the callback function
     */
    registerInvalidCertificateCallback(callback) {
        return window.__adobe_cep__.registerInvalidCertificateCallback(callback);
    }
    ;
    /**
     * Register an interest in some key events to prevent them from being sent to the host application.
     *
     * This function works with modeless extensions and panel extensions.
     * Generally all the key events will be sent to the host application for these two extensions if the current focused element
     * is not text input or dropdown,
     * If you want to intercept some key events and want them to be handled in the extension, please call this function
     * in advance to prevent them being sent to the host application.
     *
     * Since 6.1.0
     *
     * @param keyEventsInterest      A JSON string describing those key events you are interested in. A null object or
     an empty string will lead to removing the interest
     *
     * This JSON string should be an array, each object has following keys:
     *
     * keyCode:  [Required] represents an OS system dependent virtual key code identifying
     *           the unmodified value of the pressed key.
     * ctrlKey:  [optional] a Boolean that indicates if the control key was pressed (true) or not (false) when the event occurred.
     * altKey:   [optional] a Boolean that indicates if the alt key was pressed (true) or not (false) when the event occurred.
     * shiftKey: [optional] a Boolean that indicates if the shift key was pressed (true) or not (false) when the event occurred.
     * metaKey:  [optional] (Mac Only) a Boolean that indicates if the Meta key was pressed (true) or not (false) when the event occurred.
     *                      On Macintosh keyboards, this is the command key. To detect Windows key on Windows, please use keyCode instead.
     * An example JSON string:
     *
     * [
     *     {
     *         "keyCode": 48
     *     },
     *     {
     *         "keyCode": 123,
     *         "ctrlKey": true
     *     },
     *     {
     *         "keyCode": 123,
     *         "ctrlKey": true,
     *         "metaKey": true
     *     }
     * ]
     *
     */
    registerKeyEventsInterest(keyEventsInterest) {
        return window.__adobe_cep__.registerKeyEventsInterest(keyEventsInterest);
    }
    ;
    /**
     * Set the title of the extension window.
     * This function works with modal and modeless extensions in all Adobe products, and panel extensions in Photoshop, InDesign, InCopy, Illustrator, Flash Pro and Dreamweaver.
     *
     * Since 6.1.0
     *
     * @param title The window title.
     */
    setWindowTitle(title) {
        window.__adobe_cep__.invokeSync("setWindowTitle", title);
    }
    ;
    /**
     * Get the title of the extension window.
     * This function works with modal and modeless extensions in all Adobe products, and panel extensions in Photoshop, InDesign, InCopy, Illustrator, Flash Pro and Dreamweaver.
     *
     * Since 6.1.0
     *
     * @return The window title.
     */
    getWindowTitle() {
        return window.__adobe_cep__.invokeSync("getWindowTitle", "");
    }
    ;
}
/**
 * @class MenuItemStatus
 * Stores flyout menu item status
 *
 * Since 5.2.0
 */
class MenuItemStatus {
    /**
     * Stores flyout menu item status
     *
     * Since 5.2.0
     *
     * @param menuItemLabel  The menu item label.
     * @param enabled  		 True if user wants to enable the menu item.
     * @param checked  		 True if user wants to check the menu item.
     */
    constructor(menuItemLabel, enabled, checked) {
        this.menuItemLabel = menuItemLabel;
        this.enabled = enabled;
        this.checked = checked;
    }
}
/**
 * @class ContextMenuItemStatus
 * Stores the status of the context menu item.
 *
 * Since 5.2.0
 */
class ContextMenuItemStatus {
    /**
     * Stores the status of the context menu item.
     *
     * Since 5.2.0
     *
     * @param menuItemID     The menu item id.
     * @param enabled  		 True if user wants to enable the menu item.
     * @param checked  		 True if user wants to check the menu item.
     */
    constructor(menuItemID, enabled, checked) {
        this.menuItemID = menuItemID;
        this.enabled = enabled;
        this.checked = checked;
    }
}

const csInterface = new CSInterface();

function getMetaData() {
    return new Promise(resolve => {
        csInterface.evalScript("loadFileMetaDataJSON()", (json) => {
            const data = JSON.parse(json);
            resolve(data);
        });
    });
}
function setMetaData(data) {
    return new Promise(resolve => {
        const dataJSON = JSON.stringify(data);
        csInterface.evalScript(`saveFileMetaData(${dataJSON})`, () => {
            resolve();
        });
    });
}

function onUpdate(action) {
    csInterface.addEventListener("documentAfterActivate", action);
    setTimeout(action, 100);
}

var SheetFrameSource;
(function (SheetFrameSource) {
    SheetFrameSource[SheetFrameSource["FrameAnimation"] = 0] = "FrameAnimation";
    SheetFrameSource[SheetFrameSource["DocumentLayers"] = 1] = "DocumentLayers";
    SheetFrameSource[SheetFrameSource["LayerSet"] = 2] = "LayerSet";
})(SheetFrameSource || (SheetFrameSource = {}));

function getCurrentDocumentName() {
    return new Promise(resolve => {
        csInterface.evalScript("app.activeDocument.name", name => {
            resolve(name);
        });
    });
}
const sheetFileNameInput = $("#sheet_file_name");
onUpdate(() => __awaiter(undefined, void 0, void 0, function* () {
    const meta = yield getMetaData();
    sheetFileNameInput.val((meta.sheetMeta || {}).fileName || "");
}));
$("#btn_export_sheet").click(() => __awaiter(undefined, void 0, void 0, function* () {
    const metaData = yield getMetaData();
    const options = Object.assign({ documentName: "noname", atlasWidth: 0, atlasHeight: 0, frameSource: SheetFrameSource.FrameAnimation, flattenAtlas: false, layerSetIndex: 0, ignoreChildSets: false }, metaData.sheetMeta, { fileName: sheetFileNameInput.val() });
    if (!options.fileName) {
        options.fileName = (yield getCurrentDocumentName()).replace(".psd", "") + "_sheet.png";
    }
    yield setMetaData({ sheetMeta: options });
    const optionsJSON = JSON.stringify(options);
    const script = `exportSprite(${optionsJSON})`;
    csInterface.evalScript(script, response => {
        //alert(response);
    });
    sheetFileNameInput.val(options.fileName);
}));

var themeManager = (function () {
    /* Convert the Color object to string in hexadecimal format; */
    function toHex(color, delta) {
        function computeValue(value, delta) {
            var computedValue = !isNaN(delta) ? value + delta : value;
            if (computedValue < 0) {
                computedValue = 0;
            }
            else if (computedValue > 255) {
                computedValue = 255;
            }
            computedValue = Math.floor(computedValue);
            const computedValueString = computedValue.toString(16);
            return computedValueString.length === 1 ? "0" + computedValueString : computedValueString;
        }
        var hex = "";
        if (color) {
            hex = computeValue(color.red, delta) + computeValue(color.green, delta) + computeValue(color.blue, delta);
        }
        return hex;
    }
    function addRule(stylesheetId, selector, rule) {
        var stylesheet = document.getElementById(stylesheetId);
        if (stylesheet) {
            const sheet = stylesheet.sheet;
            if (sheet.addRule) {
                sheet.addRule(selector, rule);
            }
            else if (sheet.insertRule) {
                sheet.insertRule(selector + ' { ' + rule + ' }', sheet.cssRules.length);
            }
        }
    }
    /* Update the theme with the AppSkinInfo retrieved from the host product. */
    function updateThemeWithAppSkinInfo(appSkinInfo) {
        // console.log(appSkinInfo)
        var panelBgColor = appSkinInfo.panelBackgroundColor.color;
        var bgdColor = toHex(panelBgColor);
        var fontColor = "F0F0F0";
        if (panelBgColor.red > 122) {
            fontColor = "000000";
        }
        var styleId = "hostStyle";
        addRule(styleId, "body", "background-color:" + "#" + bgdColor);
        addRule(styleId, "body", "color:" + "#" + fontColor);
        var isLight = panelBgColor.red >= 127;
        if (isLight) {
            $("#theme").attr("href", "css/light.css");
        }
        else {
            $("#theme").attr("href", "css/dark.css");
        }
    }
    function onAppThemeColorChanged(event) {
        var skinInfo = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
        updateThemeWithAppSkinInfo(skinInfo);
    }
    function init() {
        var csInterface = new CSInterface();
        updateThemeWithAppSkinInfo(csInterface.hostEnvironment.appSkinInfo);
        csInterface.addEventListener(csInterface.THEME_COLOR_CHANGED_EVENT, onAppThemeColorChanged);
    }
    return {
        init: init
    };
}());
themeManager.init();

//// <reference path="../third-party/csinterface-ts.d.ts" />
const btnHori = $("#btn_horizontal");
const btnBoth = $("#btn_both");
const btnVert = $("#btn_vertical");
function setButtonBlue(btn, isBlue) {
    if (isBlue) {
        btn.addClass("topcoat-button--large--cta");
        btn.removeClass("topcoat-button--large");
    }
    else {
        btn.addClass("topcoat-button--large");
        btn.removeClass("topcoat-button--large--cta");
    }
}
function createButtonCallback(horizontal, vertical) {
    return () => {
        csInterface.evalScript(`doTilingFlip(${horizontal}, ${vertical})`, response => {
            updateButtonLook();
        });
    };
}
/*
$("#btn_flip").click(() => {
    var horizontal = $("#cb_horizontal").is(":checked");
    var vertical = $("#cb_vertical").is(":checked");

    csInterface.evalScript(`doTilingFlip(${horizontal}, ${vertical})`, () => {
        updateButtonLook();
    });
});*/
$("#btn_horizontal").click(createButtonCallback(true, false));
$("#btn_both").click(createButtonCallback(true, true));
$("#btn_vertical").click(createButtonCallback(false, true));
function updateButtonLook() {
    return __awaiter(this, void 0, void 0, function* () {
        const data = yield getMetaData();
        setButtonBlue(btnHori, data.flippedHorizontal);
        setButtonBlue(btnBoth, data.flippedHorizontal && data.flippedVertical);
        setButtonBlue(btnVert, data.flippedVertical);
    });
}
onUpdate(updateButtonLook);
