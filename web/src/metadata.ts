import { csInterface } from "./csinterface";
import { MetaData } from "./shared/metadata";

export function getMetaData(): Promise<MetaData> {
	return new Promise(resolve => {
		csInterface.evalScript("loadFileMetaDataJSON()", (json: string) => {
			const data = JSON.parse(json);
			resolve(data);
		});
	});
}


export function setMetaData(data: Partial<MetaData>): Promise<void> {
	return new Promise(resolve => {
		const dataJSON = JSON.stringify(data);

		csInterface.evalScript(`saveFileMetaData(${dataJSON})`, () => {
			resolve();
		});
	});
}