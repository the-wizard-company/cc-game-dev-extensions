import { csInterface } from "./csinterface";
import { getMetaData, setMetaData } from "./metadata";
import { onUpdate } from "./onUpdate";
import { IExportSheetOptions, SheetFrameSource } from "./shared/sheetOptions";

function getCurrentDocumentName(): Promise<string> {
	return new Promise(resolve => {
		csInterface.evalScript("app.activeDocument.name", name => {
			resolve(name);
		})
	})
}


const sheetFileNameInput = $("#sheet_file_name");

onUpdate(async () => {
	const meta = await getMetaData();
	sheetFileNameInput.val((meta.sheetMeta || {}).fileName || "");
});


$("#btn_export_sheet").click(async () => {
	const metaData = await getMetaData();
	const options: IExportSheetOptions = {
		documentName: "noname",
		atlasWidth: 0,
		atlasHeight: 0,
		frameSource: SheetFrameSource.FrameAnimation,
		flattenAtlas: false,
		layerSetIndex: 0,
		ignoreChildSets: false,
		...metaData.sheetMeta,
		fileName: sheetFileNameInput.val() as string,
	};
	if (!options.fileName) {
		options.fileName = (await getCurrentDocumentName()).replace(".psd", "") + "_sheet.png";
	}

	await setMetaData({ sheetMeta: options });

	const optionsJSON = JSON.stringify(options);
	const script = `exportSprite(${optionsJSON})`;

	csInterface.evalScript(script, response => {
		//alert(response);
	});

	sheetFileNameInput.val(options.fileName);
});

