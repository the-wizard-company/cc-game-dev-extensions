import { csInterface } from "./csinterface";

export function onUpdate(action: () => {}) {
	csInterface.addEventListener("documentAfterActivate", action);
	setTimeout(action, 100);
}