import { AppSkinInfo, CSInterface, RGBColor } from "./csinterface-ts";

var themeManager = (function () {

	/* Convert the Color object to string in hexadecimal format; */
	function toHex(color: RGBColor, delta?: number) {
		function computeValue(value: number, delta?: number) {
			var computedValue = !isNaN(delta as number) ? value + (delta as number) : value;
			if (computedValue < 0) {
				computedValue = 0;
			} else if (computedValue > 255) {
				computedValue = 255;
			}
			computedValue = Math.floor(computedValue);
			const computedValueString = computedValue.toString(16);
			return computedValueString.length === 1 ? "0" + computedValueString : computedValueString;
		}
		var hex = "";
		if (color) {
			hex = computeValue(color.red, delta) + computeValue(color.green, delta) + computeValue(color.blue, delta);
		}
		return hex;
	}

	function addRule(stylesheetId: any, selector: any, rule: any) {
		var stylesheet = document.getElementById(stylesheetId) as HTMLStyleElement;
		if (stylesheet) {
			const sheet = stylesheet.sheet as any;
			if (sheet.addRule) {
				sheet.addRule(selector, rule);
			} else if (sheet.insertRule) {
				sheet.insertRule(selector + ' { ' + rule + ' }', sheet.cssRules.length);
			}
		}
	}

	/* Update the theme with the AppSkinInfo retrieved from the host product. */
	function updateThemeWithAppSkinInfo(appSkinInfo: AppSkinInfo) {
		// console.log(appSkinInfo)
		var panelBgColor = appSkinInfo.panelBackgroundColor.color as RGBColor;
		var bgdColor = toHex(panelBgColor);
		var fontColor = "F0F0F0";
		if (panelBgColor.red > 122) {
			fontColor = "000000";
		}

		var styleId = "hostStyle";
		addRule(styleId, "body", "background-color:" + "#" + bgdColor);
		addRule(styleId, "body", "color:" + "#" + fontColor);

		var isLight = panelBgColor.red >= 127;
		if (isLight) {
			$("#theme").attr("href", "css/light.css");
		} else {
			$("#theme").attr("href", "css/dark.css");
		}
	}

	function onAppThemeColorChanged(event: any) {
		var skinInfo = JSON.parse((window as any).__adobe_cep__.getHostEnvironment()).appSkinInfo;
		updateThemeWithAppSkinInfo(skinInfo);
	}

	function init() {
		var csInterface = new CSInterface();
		updateThemeWithAppSkinInfo(csInterface.hostEnvironment.appSkinInfo);
		csInterface.addEventListener(csInterface.THEME_COLOR_CHANGED_EVENT, onAppThemeColorChanged);
	}

	return {
		init: init
	};

}());

themeManager.init();