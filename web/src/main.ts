
//// <reference path="../third-party/csinterface-ts.d.ts" />

import { csInterface } from "./csinterface";
import { getMetaData } from "./metadata";
import { onUpdate } from "./onUpdate";
import "./sheetCreator";
import "./themeManager";



const btnHori = $<HTMLButtonElement>("#btn_horizontal");
const btnBoth = $<HTMLButtonElement>("#btn_both");
const btnVert = $<HTMLButtonElement>("#btn_vertical");

function setButtonBlue(btn: JQuery<HTMLButtonElement>, isBlue: boolean) {
	if (isBlue) {
		btn.addClass("topcoat-button--large--cta");
		btn.removeClass("topcoat-button--large");
	} else {
		btn.addClass("topcoat-button--large");
		btn.removeClass("topcoat-button--large--cta");
	}
}

function createButtonCallback(horizontal: boolean, vertical: boolean) {
	return () => {
		csInterface.evalScript(`doTilingFlip(${horizontal}, ${vertical})`, response => {
			updateButtonLook();
		});
	}
}

/*
$("#btn_flip").click(() => {
	var horizontal = $("#cb_horizontal").is(":checked");
	var vertical = $("#cb_vertical").is(":checked");

	csInterface.evalScript(`doTilingFlip(${horizontal}, ${vertical})`, () => {
		updateButtonLook();
	});
});*/

$("#btn_horizontal").click(createButtonCallback(true, false));
$("#btn_both").click(createButtonCallback(true, true));
$("#btn_vertical").click(createButtonCallback(false, true));

async function updateButtonLook() {
	const data = await getMetaData();
	setButtonBlue(btnHori, data.flippedHorizontal);
	setButtonBlue(btnBoth, data.flippedHorizontal && data.flippedVertical);
	setButtonBlue(btnVert, data.flippedVertical);
}
onUpdate(updateButtonLook);