# Creative Cloud Game Dev Extensions
A collection of plugins for Adobe CC Applications, to ease game dev art creation.

## Installation

### 1. Pull this repository at the correct location
``` powershell
# for windows: open cmd and run this commands
cd C:\Users\olive\AppData\Roaming\Adobe\CEP\extensions
git clone git@gitlab.com:the-wizard-company/cc-game-dev-extensions.git
cd cc-game-dev-extensions
explorer .
```

### 2. Allow using unsigned plugins inside CC applications
On Windows simply click the `adobe-cc-allow-unsigned-applications.reg` file,
and add the keys to your registry.

### 3. Restart your creative cloud applications (Photoshop)
If you want to you can add this git repo to sourcetree, so you see if there are updates available
