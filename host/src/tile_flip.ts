/// <reference path="../types/index.d.ts" />

import { loadFileMetaData, saveFileMetaData } from "./get_file_meta";
import { _global } from "./global";
import { FlippedMeta } from "./shared/metadata";


export function doTilingFlip(horizontal: boolean, vertical: boolean) {

	//alert(horizontal + " .. " + vertical)
	const meta = loadFileMetaData() as FlippedMeta;
	if (horizontal) meta.flippedHorizontal = !meta.flippedHorizontal;
	if (vertical) meta.flippedVertical = !meta.flippedVertical;
	saveFileMetaData(meta);


	const doc = app.activeDocument;

	const layers = doc.artLayers;
	var length = doc.artLayers.length;

	const height = doc.height;
	const width = doc.width;
	const _percent50 = new UnitValue("50%");
	const _percent0 = new UnitValue("0%");

	for (var i = 0; i < length; i++) {
		const layer = doc.artLayers[i];

		if (layer.name.indexOf(":f") !== -1 && layer.name.indexOf("metadata: ") === -1) {
			//layer.applyOffset(Math.floor(width / 2), Math.floor(height / 2), OffsetUndefinedAreas.WRAPAROUND);
			layer.applyOffset(
				horizontal ? _percent50 : _percent0, 
				vertical ? _percent50 : _percent0, 
				OffsetUndefinedAreas.WRAPAROUND
			);
		}
	} 
}

_global.doTilingFlip = doTilingFlip;