$.write("before get_file_meta");

import { _global } from "./global";
import "./json";


export function getMetaLayer(): ArtLayer|null {
	const doc = app.activeDocument;
	
	const layers = doc.artLayers;
	var length = doc.artLayers.length;
	
	for (var i = 0; i < length; i++) {
		const layer = doc.artLayers[i];
		
		if (layer.name.indexOf("metadata: ") != -1) {
			return layer;
		}
	}
	return null;
}

export function getMetaLayerOrCreate(): ArtLayer {
	const doc = app.activeDocument;
	const layers = doc.artLayers;
	let layer = getMetaLayer();
	if (!layer) {
		layer = layers.add();
		layer.name = "metadata: {}";
	}
	return layer;
}


export function loadFileMetaDataJSON() {
	return JSON.stringify(loadFileMetaData());
}

export function loadFileMetaData() {
	const layer = getMetaLayer();
	if (!layer) {
		return {};
	}
	const json = layer.name.replace("metadata: ", "");
	const data = JSON.parse(json);
	return data;
}

export function saveFileMetaData(data: any) {
	const joinedData = { ...loadFileMetaData(), ...data };
	const name = "metadata: " + JSON.stringify(joinedData);
	getMetaLayerOrCreate().name = name;
}


_global.loadFileMetaDataJSON = loadFileMetaDataJSON;
_global.saveFileMetaData = saveFileMetaData;

$.write("after get_file_meta");