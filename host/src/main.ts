$.write("before main");

import "./get_file_meta";
import { _global } from "./global";
import "./spriteSheetGenerator/exportSprite";
import "./tile_flip";

$.write(_global);