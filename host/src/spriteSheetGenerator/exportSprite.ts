import { _global } from "../global";
import { IExportSheetOptions } from "../shared/sheetOptions";
import { createSpriteSheetInOtherDocument } from "./Sprite Sheet Generator 0.6.0";

$.writeln("init exportSprite before");

export function savePNGFile(document: Document, path: string) {
	// set save options
	const opts = new ExportOptionsSaveForWeb();

	opts.PNG8 = false;
	opts.transparency = true;
	opts.interlaced = false;
	opts.quality = 100;
	opts.includeProfile = false;
	opts.format = SaveDocumentType.PNG; // Document Type

	// save png file in same folder as open doc
	document.exportDocument(new (File as any)(path), ExportType.SAVEFORWEB, opts);
}

export function exportSprite(options: IExportSheetOptions) {
	const olddoc = app.activeDocument;
	// alert(options.documentName);
	const newdoc = createSpriteSheetInOtherDocument(options);

	const docPath = olddoc.path;
	const newPath = docPath + "/" + options.fileName;

	savePNGFile(newdoc, newPath);
	// alert("after-gen");
	newdoc.close(SaveOptions.DONOTSAVECHANGES);
	// alert("after close");
}

_global.exportSprite = exportSprite;

$.writeln("after exportSprite before");
