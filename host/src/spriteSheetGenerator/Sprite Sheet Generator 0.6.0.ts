﻿import { ISheetOptions, SheetFrameSource } from "../shared/sheetOptions";

// This script will export a sprite sheet as a new document given either a frame animation or a layered document.
//
// Author: John Wordsworth <john@johnwordsworth.com>
// Version: 0.6.0
// Date: June 2014
// Homepage: http://www.johnwordsworth.com/
//
// This script is released under the GPLv3 Software License. You are free to use it for free or commercial purposes.
// If you make any modifications or improvements to this script, please do share them back with us.

// Allows double clicking and opening in Photoshop

// debug level: 0-2 (0:disable, 1:break on error, 2:break at beginning)
// $.level = 1;
// debugger; // launch debugger on next line

// Constants
const kScriptTitle = "Sprite Sheet Generator 0.6.0";

const kUserOptionsKey = "userOptions";

/*
// Script Result
var gScriptResult;

// Run the Script
try {
	var result = main();

	if ( result === false ) {
		gScriptResult = 'cancel';
	} else {
		gScriptResult = 'ok';
	}
}
catch( e ) {
	if ( app.displayDialogs != DialogModes.NO )
	{
		alert(e, kScriptTitle);
	}

	gScriptResult = 'cancel';
}

gScriptResult;
*/

// Main Method
export function createSpriteSheetInOtherDocument(userOptions?: ISheetOptions) {
	entryPoint();

	if (app.documents.length === 0) {
		throw new Error("No open documents.");
	}

	// Preparation
	const srcDoc = app.activeDocument;

	if (!userOptions) {
		// Get the user's input options
		const options = getUserOptions();

		if (options === false) {
			throw new Error("no options");
		} else {
			userOptions = options;
		}
	}

	// Create the output document
	const destDoc = createDestDocWithOptions(userOptions, srcDoc);

	// If no destination doc, we didn't make a sprite sheet
	if (destDoc === false) {
		throw new Error("No destination document was created - invalid input options?");
	}

	// In the new document, we want to delete the white layer
	app.activeDocument = destDoc;
	destDoc.layers[destDoc.layers.length - 1].remove();

	// At this point the new document is a stack of frames...
	layoutLayersAsSprites(userOptions, destDoc);

	exitPoint();

	return destDoc;
}

let prevRulerUnits: Units;

function entryPoint() {
	prevRulerUnits = app.preferences.rulerUnits;

	app.preferences.rulerUnits = Units.PIXELS;
}

function exitPoint() {
	app.preferences.rulerUnits = prevRulerUnits;
}

function toTypeID(stringID: string) {
	return app.stringIDToTypeID(stringID);
}

function putActionDescValue<TKey extends keyof (ISheetOptions)>(desc: ActionDescriptor, method: string, key: TKey, value: ISheetOptions[TKey]) {
	if (key === undefined || value === undefined) {
		return;
	}

	(desc[method] as any)(toTypeID(key), value);
}

function getActionDescValue<TKey extends keyof (ISheetOptions)>(desc: ActionDescriptor, method: string, key: TKey, defaultValue: ISheetOptions[TKey]) {
	if (desc.hasKey(toTypeID(key))) {
		return (desc[method](toTypeID(key)));
	}

	return defaultValue;
}

function storeLastUserOptions(userOptions: ISheetOptions) {
	const desc = new ActionDescriptor();

	putActionDescValue(desc, "putString", "documentName", userOptions.documentName);
	putActionDescValue(desc, "putInteger", "atlasWidth", userOptions.atlasWidth);
	putActionDescValue(desc, "putInteger", "atlasHeight", userOptions.atlasHeight);
	putActionDescValue(desc, "putInteger", "frameSource", userOptions.frameSource);
	putActionDescValue(desc, "putBoolean", "flattenAtlas", userOptions.flattenAtlas);
	putActionDescValue(desc, "putInteger", "layerSetIndex", userOptions.layerSetIndex);
	putActionDescValue(desc, "putBoolean", "ignoreChildSets", userOptions.ignoreChildSets);

	app.putCustomOptions(kUserOptionsKey, desc, true);
}

function retrieveLastUserOptions(): Partial<ISheetOptions> {
	let desc: ActionDescriptor|undefined;
	try {
		desc = app.getCustomOptions(kUserOptionsKey);
	} catch (e) { return {}; }

	const result: ISheetOptions = {} as ISheetOptions;

	result.documentName = getActionDescValue(desc, "getString", "documentName", "");
	result.atlasWidth = getActionDescValue(desc, "getInteger", "atlasWidth", 0);
	result.atlasHeight = getActionDescValue(desc, "getInteger", "atlasHeight", 0);
	result.frameSource = getActionDescValue(desc, "getInteger", "frameSource", 0);
	result.flattenAtlas = getActionDescValue(desc, "getBoolean", "flattenAtlas", false);
	result.layerSetIndex = getActionDescValue(desc, "getInteger", "layerSetIndex", 0);
	result.ignoreChildSets = getActionDescValue(desc, "getBoolean", "ignoreChildSets", true);

	return result;
}

function indexOf<T>(array: T[], obj: T) {
	for (let k = 0; k < array.length; ++k) {
		if (array[k] === obj) {
			return k;
		}
	}

	return -1;
}

function getUserOptions(): ISheetOptions | false {
	// Note: When running from ExtendScript editor, editing the script soetimes destroys all previous options!
	const lastUserOptions = retrieveLastUserOptions();

	// If dialogs are turned off, we just return the last / default options
	// if ( app.playbackDisplayDialogs != DialogModes.ALL ) {
	//    return lastUserOptions;
	// }

	const sizeItems = ["Auto", 32, 64, 128, 256, 512, 1024, 2048, 4096];
	const sourceItems = ["Frame Animation", "Document Root Layers", "Specific Layer Set"];
	const layerSetItems = [];

	for (let k = 0; k < app.activeDocument.layerSets.length; ++k) {
		const layerSet = app.activeDocument.layerSets[k];
		layerSetItems[layerSetItems.length] = layerSet.name;
	}

	if (layerSetItems.length === 0) { layerSetItems[0] = "No Layer Sets Available"; }

	const dlg = new (Window as any)("dialog", kScriptTitle);

	dlg.atlasSizeTitleGrp = dlg.add("group", undefined);
	dlg.atlasSizeTitleGrp.alignment = "left";
	dlg.atlasSizeTitleGrp.label01 = dlg.atlasSizeTitleGrp.add("StaticText", undefined, "Atlas Size (Pixels)");

	dlg.atlasSizeGrp = dlg.add("group", undefined);
	dlg.atlasSizeGrp.orientation = "row";
	dlg.atlasSizeGrp.alignment = "left";
	dlg.atlasSizeGrp.dropDownWidth = dlg.atlasSizeGrp.add("DropDownList", undefined, sizeItems);
	dlg.atlasSizeGrp.dropDownWidth.text = "Width: ";
	dlg.atlasSizeGrp.dropDownWidth.selection = 0;
	dlg.atlasSizeGrp.dropDownHeight = dlg.atlasSizeGrp.add("DropDownList", undefined, sizeItems);
	dlg.atlasSizeGrp.dropDownHeight.text = "Height: ";
	dlg.atlasSizeGrp.dropDownHeight.selection = 0;

	dlg.sourceTitleGrp = dlg.add("group", undefined);
	dlg.sourceTitleGrp.alignment = "left";
	dlg.sourceTitleGrp.label01 = dlg.sourceTitleGrp.add("StaticText", undefined, "Sprite Source");

	dlg.sourceGrp = dlg.add("group", undefined);
	dlg.sourceGrp.orientation = "column";
	dlg.sourceGrp.alignment = "left";
	dlg.sourceGrp.dropDownSource = dlg.sourceGrp.add("DropDownList", undefined, sourceItems);
	dlg.sourceGrp.dropDownSource.text = "Source: ";
	dlg.sourceGrp.dropDownSource.selection = 0;
	dlg.sourceGrp.dropDownSource.onChange = () => {
		userOptionsDlgUpdateLayerSetChildControls(dlg);
	};

	dlg.sourceGrp.dropDownLayerSet = dlg.sourceGrp.add("DropDownList", undefined, layerSetItems);
	dlg.sourceGrp.dropDownLayerSet.text = "Layer Set: ";
	dlg.sourceGrp.dropDownLayerSet.selection = 0;

	dlg.sourceGrp.ignoreChildSetsCheck = dlg.sourceGrp.add("CheckBox", undefined, "Ignore Child Layer Sets");
	dlg.sourceGrp.ignoreChildSetsCheck.value = true;

	dlg.otherTitleGrp = dlg.add("group", undefined);
	dlg.otherTitleGrp.alignment = "left";
	dlg.otherTitleGrp.label01 = dlg.otherTitleGrp.add("StaticText", undefined, "Other Options");

	dlg.flattenGrp = dlg.add("group");
	dlg.flattenGrp.orientation = "row";
	dlg.flattenGrp.alignment = "left";
	dlg.flattenGrp.flattenCheck = dlg.flattenGrp.add("CheckBox", undefined, "Flatten Image?");

	dlg.buttonGrp = dlg.add("group");
	dlg.buttonGrp.okButton = dlg.buttonGrp.add("button", undefined, "OK");
	dlg.buttonGrp.cancelButton = dlg.buttonGrp.add("button", undefined, "Cancel");

	// Before presenting, update from user options (if applicable)
	if (lastUserOptions.atlasWidth && lastUserOptions.atlasWidth > 0) { dlg.atlasSizeGrp.dropDownWidth.selection = indexOf(sizeItems, lastUserOptions.atlasWidth); }
	if (lastUserOptions.atlasHeight && lastUserOptions.atlasHeight > 0) { dlg.atlasSizeGrp.dropDownHeight.selection = indexOf(sizeItems, lastUserOptions.atlasHeight); }
	if (lastUserOptions.frameSource && lastUserOptions.frameSource > 0) { dlg.sourceGrp.dropDownSource.selection = lastUserOptions.frameSource; }
	if (lastUserOptions.flattenAtlas !== undefined) { dlg.flattenGrp.flattenCheck.value = lastUserOptions.flattenAtlas; }
	if (lastUserOptions.ignoreChildSets !== undefined) { dlg.sourceGrp.ignoreChildSetsCheck.value = lastUserOptions.ignoreChildSets; }

	if ((lastUserOptions.documentName === app.activeDocument.name) && (lastUserOptions.layerSetIndex! < app.activeDocument.layerSets.length)) {
		dlg.sourceGrp.dropDownLayerSet.selection = lastUserOptions.layerSetIndex;
	}

	userOptionsDlgUpdateLayerSetChildControls(dlg);
	dlg.center();
	const dlgResult = dlg.show(); // 1 = ok, 2 = cancel

	// Note: .selection as a string is the value in the drop down, as an int it's the item's index!
	const userOptions: ISheetOptions = {} as ISheetOptions;
	userOptions.documentName = app.activeDocument.name;
	userOptions.atlasWidth = parseInt(sizeItems[parseInt(dlg.atlasSizeGrp.dropDownWidth.selection as string)] as string);
	userOptions.atlasHeight = parseInt(sizeItems[parseInt(dlg.atlasSizeGrp.dropDownHeight.selection as string)] as string);
	userOptions.frameSource = parseInt(dlg.sourceGrp.dropDownSource.selection);
	userOptions.flattenAtlas = dlg.flattenGrp.flattenCheck.value;
	userOptions.layerSetIndex = parseInt(dlg.sourceGrp.dropDownLayerSet.selection);
	userOptions.ignoreChildSets = dlg.sourceGrp.ignoreChildSetsCheck.value;

	if (isNaN(userOptions.atlasWidth)) { userOptions.atlasWidth = 0; }
	if (isNaN(userOptions.atlasHeight)) { userOptions.atlasHeight = 0; }

	storeLastUserOptions(userOptions);

	if (dlgResult === 2) {
		return false;
	}

	return userOptions;
}

function userOptionsDlgUpdateLayerSetChildControls(dlg: any) {
	if (dlg.sourceGrp.dropDownSource.selection === SheetFrameSource.LayerSet) {
		dlg.sourceGrp.dropDownLayerSet.visible = true;
	} else {
		dlg.sourceGrp.dropDownLayerSet.visible = false;
	}

	if (dlg.sourceGrp.dropDownSource.selection === SheetFrameSource.FrameAnimation) {
		dlg.sourceGrp.ignoreChildSetsCheck.visible = false;
	} else {
		dlg.sourceGrp.ignoreChildSetsCheck.visible = true;
	}
}

function createDestDocWithOptions(userOptions: ISheetOptions | false, srcDoc: Document) {
	if (userOptions === false) {
		return false;
	}

	const sourceType = userOptions.frameSource;
	const layerSetIndex = userOptions.layerSetIndex;
	let destDoc: false | Document = false;

	if (sourceType === SheetFrameSource.FrameAnimation) {
		// How many frames in the animation?
		const frameCount = getFrameCount();

		if (frameCount === 0) {
			throw new Error("No animation frames were found in this file. In 'Frame Animation' mode this script requires a Photo Frame animation to create a sprite sheet.");
		}

		destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
		copyAnimationFrames(userOptions, srcDoc, destDoc, frameCount);
	} else if (sourceType === SheetFrameSource.DocumentLayers) {
		destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
		copyLayersAsSprites(userOptions, srcDoc, destDoc, srcDoc.layers);
	} else if (sourceType === SheetFrameSource.LayerSet) {
		if (layerSetIndex >= srcDoc.layerSets.length) {
			throw new Error("The 'Specific Layer Set' option was selected, but there is no selected layer set in the open document.");
		}

		if (srcDoc.layerSets[layerSetIndex].layers.length === 0) {
			throw new Error("The selected layer set contains no layers.");
		}

		destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
		copyLayersAsSprites(userOptions, srcDoc, destDoc, srcDoc.layerSets[layerSetIndex].layers);
	}

	app.activeDocument = destDoc as Document;
	return destDoc;
}

function copyAnimationFrames(userOptions: ISheetOptions, srcDoc: Document, destDoc: Document, frameCount: number) {
	// Copy Merged Frames across to new image
	for (let k = 1; k <= frameCount; k++) {
		app.activeDocument = srcDoc;
		goToFrame(k);

		// original code:
		// selectAllLayers();
		// duplicateSelectedLayers();
		// mergeSelectedLayers();
		srcDoc.mergeVisibleLayers();
		const layer = srcDoc.activeLayer.duplicate(destDoc as any, ElementPlacement.PLACEATEND);

		// go back the mergeVisibleLayers step
		const idRvrt = app.charIDToTypeID("Rvrt");
		app.executeAction(idRvrt, undefined, DialogModes.NO);

		deleteSelectedLayers();

		app.activeDocument = destDoc;
		(layer as any).name = "Frame " + k;
	}
}

function copyLayersAsSprites(userOptions: ISheetOptions, srcDoc: Document, destDoc: Document, layers: Layers) {
	app.activeDocument = srcDoc;

	if (srcDoc === undefined || destDoc === undefined || layers === undefined) {
		throw new Error("copyLayersAsSprites called with undefined parameter(s).");
	}

	const wasVisible = [];
	let bgLayerIndex = -1;
	let wasParentVisible;

	// Pre process
	for (let k = 0; k < layers.length; k++) {
		wasVisible[k] = layers[k].visible;

		if ((layers[k] as ArtLayer).isBackgroundLayer) {
			bgLayerIndex = k;
			(layers[k] as ArtLayer).isBackgroundLayer = false;
			layers[k].visible = wasVisible[k]; // Toggling background layer affects visibility
		}
	}

	if ((layers[0].parent as any).visible !== undefined) {
		wasParentVisible = (layers[0].parent as any).visible;
		(layers[0].parent as any).visible = true;
	}

	// Do the work
	for (let k = 0; k < layers.length; k++) {
		app.activeDocument = srcDoc;

		// Don't process background layer as it causes issues (probably don't want it as a sprite anyway)
		if (k === bgLayerIndex) {
			continue;
		}

		// If we are ignoring child sets, check and skip if necessary
		if (userOptions.ignoreChildSets && (layers[k] as any).layers !== undefined && (layers[k] as any).layers.length > 0) {
			continue;
		}

		// Turn off all layers (but don't mess with the background layer)
		for (let j = 0; j < layers.length; j++) {
			if (j !== bgLayerIndex) {
				layers[j].visible = false;
			}
		}

		// Turn on active layer
		layers[k].visible = true;

		// Turn into sprite!
		selectAllLayers();
		duplicateSelectedLayers();
		mergeSelectedLayers();
		const layer = srcDoc.activeLayer.duplicate(destDoc as any, ElementPlacement.PLACEATEND);
		deleteSelectedLayers();

		app.activeDocument = destDoc;
		(layer as any).name = "Sprite " + (k + 1);
	}

	// Undo Pre-Process
	app.activeDocument = srcDoc;

	for (let k = 0; k < layers.length; k++) {
		if (k === bgLayerIndex) {
			(layers[k] as ArtLayer).isBackgroundLayer = true;
		}
	}

	for (let k = 0; k < layers.length; k++) {
		layers[k].visible = wasVisible[k];
	}

	if ((layers[0].parent as any).visible !== undefined) {
		(layers[0].parent as any).visible = wasParentVisible;
	}
}

function atlasSizeForFrames(userOptions: ISheetOptions, frameWidth: number, frameHeight: number, frameCount: number) {
	let atlasWidth = 16;
	let atlasHeight = 16;
	let didFit = false;
	let fixedWidth = false;
	let fixedHeight = false;

	if (userOptions.atlasWidth > 0) { atlasWidth = userOptions.atlasWidth; fixedWidth = true; }
	if (userOptions.atlasHeight > 0) { atlasHeight = userOptions.atlasHeight; fixedHeight = true; }

	if (fixedWidth && fixedHeight) {
		return { width: atlasWidth, height: atlasHeight };
	}

	while (didFit === false) {
		const x = 0;
		const y = 0;

		// $.writeln("Trying to fit " + frameCount + "x" + frameWidth + "x" + frameHeight + " onto: " + atlasWidth + " x " + atlasHeight);

		const gridWidth = Math.floor(atlasWidth / frameWidth);
		const requiredGridHeight = Math.ceil(frameCount / gridWidth);
		const requiredPixelHeight = requiredGridHeight * frameHeight;

		if (requiredPixelHeight > atlasHeight) {
			if ((atlasWidth <= atlasHeight) && (fixedWidth === false)) { atlasWidth *= 2; } else { atlasHeight *= 2; }

			didFit = false;
		} else {
			didFit = true;
		}
	}

	const result = { width: atlasWidth, height: atlasHeight };
	// $.writeln("Atlas Size: " + result.width + " x " + result.height);

	return result;
}

function layoutLayersAsSprites(userOptions: ISheetOptions, destDoc: Document) {
	app.activeDocument = destDoc;

	// Figure out atlas size
	const frameWidth = parseInt(destDoc.width as any);
	const frameHeight = parseInt(destDoc.height as any);
	const frameCount = destDoc.layers.length;
	const atlasSize = atlasSizeForFrames(userOptions, frameWidth, frameHeight, frameCount);
	destDoc.resizeCanvas(atlasSize.width as any, atlasSize.height as any, AnchorPosition.TOPLEFT);

	// Layout Layers
	let x = 0;
	let y = 0;

	for (let k = 0; k < frameCount; ++k) {
		try {
			destDoc.layers[k].translate(x as any, y as any);
		} catch (e) { /* ignore error */ }

		x += frameWidth;

		if ((x + frameWidth) > atlasSize.width) {
			x = 0;
			y += frameHeight;
		}
	}

	// Layout sprites
	for (let k = 0; k < frameCount; k++) {
		// tslint:disable-next-line:no-unused-expression
		destDoc.layers[k];
	}

	if (userOptions.flattenAtlas === true) {
		selectAllLayers();
		mergeSelectedLayers();
	}
}

//
// Animation Actions
//

// Count the number of frames in the current frame animation (brute force, slow).
function getFrameCount() {
	for (let k = 1; k < 999; k++) {
		if (goToFrame(k) === false) {
			return k - 1;
		}
	}

	return 0;
}

// Jump to the given frame in the frame animation in the active document
function goToFrame(jumpToFrame: number) {
	try {
		const desc = new ActionDescriptor();
		const ref1 = new ActionReference();
		ref1.putIndex(app.stringIDToTypeID("animationFrameClass"), jumpToFrame);
		desc.putReference(app.charIDToTypeID("null"), ref1);
		app.executeAction(app.charIDToTypeID("slct"), desc, DialogModes.NO);

		return true;

	} catch (e) {
		// $.writeln(e);
	}

	return false;
}

//
// Layer Editing Actions
//

// Select all layers in the active document
function selectAllLayers() {
	const idselectAllLayers = app.stringIDToTypeID("selectAllLayers");
	const desc4 = new ActionDescriptor();
	const idnull = app.charIDToTypeID("null");
	const ref1 = new ActionReference();
	const idLyr = app.charIDToTypeID("Lyr ");
	const idOrdn = app.charIDToTypeID("Ordn");
	const idTrgt = app.charIDToTypeID("Trgt");
	ref1.putEnumerated(idLyr, idOrdn, idTrgt);
	desc4.putReference(idnull, ref1);
	app.executeAction(idselectAllLayers, desc4, DialogModes.NO);
}

// Duplicate all layers that are currently selected
function duplicateSelectedLayers() {
	const idDplc = app.charIDToTypeID("Dplc");
	const desc5 = new ActionDescriptor();
	const idnull = app.charIDToTypeID("null");
	const ref2 = new ActionReference();
	const idLyr = app.charIDToTypeID("Lyr ");
	const idOrdn = app.charIDToTypeID("Ordn");
	const idTrgt = app.charIDToTypeID("Trgt");
	ref2.putEnumerated(idLyr, idOrdn, idTrgt);
	desc5.putReference(idnull, ref2);
	const idVrsn = app.charIDToTypeID("Vrsn");
	desc5.putInteger(idVrsn, 2);
	app.executeAction(idDplc, desc5, DialogModes.NO);
}

// Merge all currently selected layers
function mergeSelectedLayers() {
	const idMrgtwo = app.charIDToTypeID("Mrg2");

	try {
		app.executeAction(idMrgtwo, undefined, DialogModes.NO);
	} catch (e) { /* ignore error */ }
}

// Delete all selected layers
function deleteSelectedLayers() {
	// Delete selected layers
	const idDlt = app.charIDToTypeID("Dlt ");
	const desc8 = new ActionDescriptor();
	const idnull = app.charIDToTypeID("null");
	const ref6 = new ActionReference();
	const idLyr = app.charIDToTypeID("Lyr ");
	const idOrdn = app.charIDToTypeID("Ordn");
	const idTrgt = app.charIDToTypeID("Trgt");
	ref6.putEnumerated(idLyr, idOrdn, idTrgt);
	desc8.putReference(idnull, ref6);

	try {
		app.executeAction(idDlt, desc8, DialogModes.NO);
	} catch (e) { /* ignore error */ }
}
