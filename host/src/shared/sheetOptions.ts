export interface ISheetOptions {
	documentName: string;
	atlasWidth: number;
	atlasHeight: number;
	frameSource: SheetFrameSource; //enum
	flattenAtlas: boolean;
	layerSetIndex: number;
	ignoreChildSets: boolean;
}

export enum SheetFrameSource {
	FrameAnimation = 0,
	DocumentLayers = 1,
	LayerSet = 2,
}

export interface IExportSheetOptions extends ISheetOptions {
	fileName: string;
}