import { IExportSheetOptions } from "./sheetOptions";

export interface FlippedMeta {
	flippedHorizontal: boolean;
	flippedVertical: boolean;
}

export interface SheetMetaData {
	sheetMeta: IExportSheetOptions;
}

export type MetaData = FlippedMeta & SheetMetaData;