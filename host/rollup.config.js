import typescript from 'rollup-plugin-typescript2';
import pkg from './package.json';


export default {
	input: 'src/main.ts',
	output: [
		{
			file: pkg.main,
			format: 'cjs',
		},
	],
	external: [
		...Object.keys(pkg.dependencies || {}),
		...Object.keys(pkg.peerDependencies || {}),
	],

	plugins: [
		typescript({
			typescript: require('typescript'),
		}),
	],
	treeshaking: false,
	useStrict: false,
}