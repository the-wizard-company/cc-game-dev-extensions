'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

// not actually global, but it prevents tree shaking
var _global = {};

//  json2.js
//  2017-06-12
//  Public Domain.
//  NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
//  USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
//  NOT CONTROL.
//  This file creates a global JSON object containing two methods: stringify
//  and parse. This file provides the ES5 JSON capability to ES3 systems.
//  If a project might run on IE8 or earlier, then this file should be included.
//  This file does nothing on ES5 systems.
//      JSON.stringify(value, replacer, space)
//          value       any JavaScript value, usually an object or array.
//          replacer    an optional parameter that determines how object
//                      values are stringified for objects. It can be a
//                      function or an array of strings.
//          space       an optional parameter that specifies the indentation
//                      of nested structures. If it is omitted, the text will
//                      be packed without extra whitespace. If it is a number,
//                      it will specify the number of spaces to indent at each
//                      level. If it is a string (such as "\t" or "&nbsp;"),
//                      it contains the characters used to indent at each level.
//          This method produces a JSON text from a JavaScript value.
//          When an object value is found, if the object contains a toJSON
//          method, its toJSON method will be called and the result will be
//          stringified. A toJSON method does not serialize: it returns the
//          value represented by the name/value pair that should be serialized,
//          or undefined if nothing should be serialized. The toJSON method
//          will be passed the key associated with the value, and this will be
//          bound to the value.
//          For example, this would serialize Dates as ISO strings.
//              Date.prototype.toJSON = function (key) {
//                  function f(n) {
//                      // Format integers to have at least two digits.
//                      return (n < 10)
//                          ? "0" + n
//                          : n;
//                  }
//                  return this.getUTCFullYear()   + "-" +
//                       f(this.getUTCMonth() + 1) + "-" +
//                       f(this.getUTCDate())      + "T" +
//                       f(this.getUTCHours())     + ":" +
//                       f(this.getUTCMinutes())   + ":" +
//                       f(this.getUTCSeconds())   + "Z";
//              };
//          You can provide an optional replacer method. It will be passed the
//          key and value of each member, with this bound to the containing
//          object. The value that is returned from your method will be
//          serialized. If your method returns undefined, then the member will
//          be excluded from the serialization.
//          If the replacer parameter is an array of strings, then it will be
//          used to select the members to be serialized. It filters the results
//          such that only members with keys listed in the replacer array are
//          stringified.
//          Values that do not have JSON representations, such as undefined or
//          functions, will not be serialized. Such values in objects will be
//          dropped; in arrays they will be replaced with null. You can use
//          a replacer function to replace those with JSON values.
//          JSON.stringify(undefined) returns undefined.
//          The optional space parameter produces a stringification of the
//          value that is filled with line breaks and indentation to make it
//          easier to read.
//          If the space parameter is a non-empty string, then that string will
//          be used for indentation. If the space parameter is a number, then
//          the indentation will be that many spaces.
//          Example:
//          text = JSON.stringify(["e", {pluribus: "unum"}]);
//          // text is '["e",{"pluribus":"unum"}]'
//          text = JSON.stringify(["e", {pluribus: "unum"}], null, "\t");
//          // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'
//          text = JSON.stringify([new Date()], function (key, value) {
//              return this[key] instanceof Date
//                  ? "Date(" + this[key] + ")"
//                  : value;
//          });
//          // text is '["Date(---current time---)"]'
//      JSON.parse(text, reviver)
//          This method parses a JSON text to produce an object or array.
//          It can throw a SyntaxError exception.
//          The optional reviver parameter is a function that can filter and
//          transform the results. It receives each of the keys and values,
//          and its return value is used instead of the original value.
//          If it returns what it received, then the structure is not modified.
//          If it returns undefined then the member is deleted.
//          Example:
//          // Parse the text. Values that look like ISO date strings will
//          // be converted to Date objects.
//          myData = JSON.parse(text, function (key, value) {
//              var a;
//              if (typeof value === "string") {
//                  a =
//   /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
//                  if (a) {
//                      return new Date(Date.UTC(
//                         +a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]
//                      ));
//                  }
//                  return value;
//              }
//          });
//          myData = JSON.parse(
//              "[\"Date(09/09/2001)\"]",
//              function (key, value) {
//                  var d;
//                  if (
//                      typeof value === "string"
//                      && value.slice(0, 5) === "Date("
//                      && value.slice(-1) === ")"
//                  ) {
//                      d = new Date(value.slice(5, -1));
//                      if (d) {
//                          return d;
//                      }
//                  }
//                  return value;
//              }
//          );
//  This is a reference implementation. You are free to copy, modify, or
//  redistribute.
/*jslint
    eval, for, this
*/
/*property
    JSON, apply, call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/
// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.
if (typeof JSON !== "object") {
    JSON = {};
}
(function () {
    var rx_one = /^[\],:{}\s]*$/;
    var rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
    var rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
    var rx_four = /(?:^|:|,)(?:\s*\[)+/g;
    var rx_escapable = /[\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
    var rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
    function f(n) {
        // Format integers to have at least two digits.
        return (n < 10)
            ? "0" + n
            : n;
    }
    function this_value() {
        return this.valueOf();
    }
    if (typeof Date.prototype.toJSON !== "function") {
        Date.prototype.toJSON = function () {
            return isFinite(this.valueOf())
                ? (this.getUTCFullYear()
                    + "-"
                    + f(this.getUTCMonth() + 1)
                    + "-"
                    + f(this.getUTCDate())
                    + "T"
                    + f(this.getUTCHours())
                    + ":"
                    + f(this.getUTCMinutes())
                    + ":"
                    + f(this.getUTCSeconds())
                    + "Z")
                : null;
        };
        Boolean.prototype.toJSON = this_value;
        Number.prototype.toJSON = this_value;
        String.prototype.toJSON = this_value;
    }
    var gap;
    var indent;
    var meta;
    var rep;
    function quote(string) {
        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.
        rx_escapable.lastIndex = 0;
        return rx_escapable.test(string)
            ? "\"" + string.replace(rx_escapable, function (a) {
                var c = meta[a];
                return typeof c === "string"
                    ? c
                    : "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4);
            }) + "\""
            : "\"" + string + "\"";
    }
    function str(key, holder) {
        // Produce a string from holder[key].
        var i; // The loop counter.
        var k; // The member key.
        var v; // The member value.
        var length;
        var mind = gap;
        var partial;
        var value = holder[key];
        // If the value has a toJSON method, call it to obtain a replacement value.
        if (value
            && typeof value === "object"
            && typeof value.toJSON === "function") {
            value = value.toJSON(key);
        }
        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.
        if (typeof rep === "function") {
            value = rep.call(holder, key, value);
        }
        // What happens next depends on the value's type.
        switch (typeof value) {
            case "string":
                return quote(value);
            case "number":
                // JSON numbers must be finite. Encode non-finite numbers as null.
                return (isFinite(value))
                    ? String(value)
                    : "null";
            case "boolean":
            case "null":
                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce "null". The case is included here in
                // the remote chance that this gets fixed someday.
                return String(value);
            // If the type is "object", we might be dealing with an object or an array or
            // null.
            case "object":
                // Due to a specification blunder in ECMAScript, typeof null is "object",
                // so watch out for that case.
                if (!value) {
                    return "null";
                }
                // Make an array to hold the partial results of stringifying this object value.
                gap += indent;
                partial = [];
                // Is the value an array?
                if (Object.prototype.toString.apply(value) === "[object Array]") {
                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON values.
                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || "null";
                    }
                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.
                    v = partial.length === 0
                        ? "[]"
                        : gap
                            ? ("[\n"
                                + gap
                                + partial.join(",\n" + gap)
                                + "\n"
                                + mind
                                + "]")
                            : "[" + partial.join(",") + "]";
                    gap = mind;
                    return v;
                }
                // If the replacer is an array, use it to select the members to be stringified.
                if (rep && typeof rep === "object") {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        if (typeof rep[i] === "string") {
                            k = rep[i];
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + ((gap)
                                    ? ": "
                                    : ":") + v);
                            }
                        }
                    }
                }
                else {
                    // Otherwise, iterate through all of the keys in the object.
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + ((gap)
                                    ? ": "
                                    : ":") + v);
                            }
                        }
                    }
                }
                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.
                v = partial.length === 0
                    ? "{}"
                    : gap
                        ? "{\n" + gap + partial.join(",\n" + gap) + "\n" + mind + "}"
                        : "{" + partial.join(",") + "}";
                gap = mind;
                return v;
        }
    }
    // If the JSON object does not yet have a stringify method, give it one.
    if (typeof JSON.stringify !== "function") {
        meta = {
            "\b": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\f": "\\f",
            "\r": "\\r",
            "\"": "\\\"",
            "\\": "\\\\"
        };
        JSON.stringify = function (value, replacer, space) {
            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.
            var i;
            gap = "";
            indent = "";
            // If the space parameter is a number, make an indent string containing that
            // many spaces.
            if (typeof space === "number") {
                for (i = 0; i < space; i += 1) {
                    indent += " ";
                }
                // If the space parameter is a string, it will be used as the indent string.
            }
            else if (typeof space === "string") {
                indent = space;
            }
            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.
            rep = replacer;
            if (replacer && typeof replacer !== "function" && (typeof replacer !== "object"
                || typeof replacer.length !== "number")) {
                throw new Error("JSON.stringify");
            }
            // Make a fake root object containing our value under the key of "".
            // Return the result of stringifying the value.
            return str("", { "": value });
        };
    }
    // If the JSON object does not yet have a parse method, give it one.
    if (typeof JSON.parse !== "function") {
        JSON.parse = function (text, reviver) {
            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.
            var j;
            function walk(holder, key) {
                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.
                var k;
                var v;
                var value = holder[key];
                if (value && typeof value === "object") {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            }
                            else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }
            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.
            text = String(text);
            rx_dangerous.lastIndex = 0;
            if (rx_dangerous.test(text)) {
                text = text.replace(rx_dangerous, function (a) {
                    return ("\\u"
                        + ("0000" + a.charCodeAt(0).toString(16)).slice(-4));
                });
            }
            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with "()" and "new"
            // because they can cause invocation, and "=" because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.
            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with "@" (a non-JSON character). Second, we
            // replace all simple value tokens with "]" characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or "]" or
            // "," or ":" or "{" or "}". If that is so, then the text is safe for eval.
            if (rx_one.test(text
                .replace(rx_two, "@")
                .replace(rx_three, "]")
                .replace(rx_four, ""))) {
                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The "{" operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.
                j = eval("(" + text + ")");
                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.
                return (typeof reviver === "function")
                    ? walk({ "": j }, "")
                    : j;
            }
            // If the text is not JSON parseable, then a SyntaxError is thrown.
            throw new SyntaxError("JSON.parse");
        };
    }
}());

$.write("before get_file_meta");
function getMetaLayer() {
    var doc = app.activeDocument;
    var layers = doc.artLayers;
    var length = doc.artLayers.length;
    for (var i = 0; i < length; i++) {
        var layer = doc.artLayers[i];
        if (layer.name.indexOf("metadata: ") != -1) {
            return layer;
        }
    }
    return null;
}
function getMetaLayerOrCreate() {
    var doc = app.activeDocument;
    var layers = doc.artLayers;
    var layer = getMetaLayer();
    if (!layer) {
        layer = layers.add();
        layer.name = "metadata: {}";
    }
    return layer;
}
function loadFileMetaDataJSON() {
    return JSON.stringify(loadFileMetaData());
}
function loadFileMetaData() {
    var layer = getMetaLayer();
    if (!layer) {
        return {};
    }
    var json = layer.name.replace("metadata: ", "");
    var data = JSON.parse(json);
    return data;
}
function saveFileMetaData(data) {
    var joinedData = __assign({}, loadFileMetaData(), data);
    var name = "metadata: " + JSON.stringify(joinedData);
    getMetaLayerOrCreate().name = name;
}
_global.loadFileMetaDataJSON = loadFileMetaDataJSON;
_global.saveFileMetaData = saveFileMetaData;
$.write("after get_file_meta");

var SheetFrameSource;
(function (SheetFrameSource) {
    SheetFrameSource[SheetFrameSource["FrameAnimation"] = 0] = "FrameAnimation";
    SheetFrameSource[SheetFrameSource["DocumentLayers"] = 1] = "DocumentLayers";
    SheetFrameSource[SheetFrameSource["LayerSet"] = 2] = "LayerSet";
})(SheetFrameSource || (SheetFrameSource = {}));

// This script will export a sprite sheet as a new document given either a frame animation or a layered document.
//
// Author: John Wordsworth <john@johnwordsworth.com>
// Version: 0.6.0
// Date: June 2014
// Homepage: http://www.johnwordsworth.com/
//
// This script is released under the GPLv3 Software License. You are free to use it for free or commercial purposes.
// If you make any modifications or improvements to this script, please do share them back with us.
// Allows double clicking and opening in Photoshop
// debug level: 0-2 (0:disable, 1:break on error, 2:break at beginning)
// $.level = 1;
// debugger; // launch debugger on next line
// Constants
var kScriptTitle = "Sprite Sheet Generator 0.6.0";
var kUserOptionsKey = "userOptions";
/*
// Script Result
var gScriptResult;

// Run the Script
try {
    var result = main();

    if ( result === false ) {
        gScriptResult = 'cancel';
    } else {
        gScriptResult = 'ok';
    }
}
catch( e ) {
    if ( app.displayDialogs != DialogModes.NO )
    {
        alert(e, kScriptTitle);
    }

    gScriptResult = 'cancel';
}

gScriptResult;
*/
// Main Method
function createSpriteSheetInOtherDocument(userOptions) {
    entryPoint();
    if (app.documents.length === 0) {
        throw new Error("No open documents.");
    }
    // Preparation
    var srcDoc = app.activeDocument;
    if (!userOptions) {
        // Get the user's input options
        var options = getUserOptions();
        if (options === false) {
            throw new Error("no options");
        }
        else {
            userOptions = options;
        }
    }
    // Create the output document
    var destDoc = createDestDocWithOptions(userOptions, srcDoc);
    // If no destination doc, we didn't make a sprite sheet
    if (destDoc === false) {
        throw new Error("No destination document was created - invalid input options?");
    }
    // In the new document, we want to delete the white layer
    app.activeDocument = destDoc;
    destDoc.layers[destDoc.layers.length - 1].remove();
    // At this point the new document is a stack of frames...
    layoutLayersAsSprites(userOptions, destDoc);
    exitPoint();
    return destDoc;
}
var prevRulerUnits;
function entryPoint() {
    prevRulerUnits = app.preferences.rulerUnits;
    app.preferences.rulerUnits = Units.PIXELS;
}
function exitPoint() {
    app.preferences.rulerUnits = prevRulerUnits;
}
function toTypeID(stringID) {
    return app.stringIDToTypeID(stringID);
}
function putActionDescValue(desc, method, key, value) {
    if (key === undefined || value === undefined) {
        return;
    }
    desc[method](toTypeID(key), value);
}
function getActionDescValue(desc, method, key, defaultValue) {
    if (desc.hasKey(toTypeID(key))) {
        return (desc[method](toTypeID(key)));
    }
    return defaultValue;
}
function storeLastUserOptions(userOptions) {
    var desc = new ActionDescriptor();
    putActionDescValue(desc, "putString", "documentName", userOptions.documentName);
    putActionDescValue(desc, "putInteger", "atlasWidth", userOptions.atlasWidth);
    putActionDescValue(desc, "putInteger", "atlasHeight", userOptions.atlasHeight);
    putActionDescValue(desc, "putInteger", "frameSource", userOptions.frameSource);
    putActionDescValue(desc, "putBoolean", "flattenAtlas", userOptions.flattenAtlas);
    putActionDescValue(desc, "putInteger", "layerSetIndex", userOptions.layerSetIndex);
    putActionDescValue(desc, "putBoolean", "ignoreChildSets", userOptions.ignoreChildSets);
    app.putCustomOptions(kUserOptionsKey, desc, true);
}
function retrieveLastUserOptions() {
    var desc;
    try {
        desc = app.getCustomOptions(kUserOptionsKey);
    }
    catch (e) {
        return {};
    }
    var result = {};
    result.documentName = getActionDescValue(desc, "getString", "documentName", "");
    result.atlasWidth = getActionDescValue(desc, "getInteger", "atlasWidth", 0);
    result.atlasHeight = getActionDescValue(desc, "getInteger", "atlasHeight", 0);
    result.frameSource = getActionDescValue(desc, "getInteger", "frameSource", 0);
    result.flattenAtlas = getActionDescValue(desc, "getBoolean", "flattenAtlas", false);
    result.layerSetIndex = getActionDescValue(desc, "getInteger", "layerSetIndex", 0);
    result.ignoreChildSets = getActionDescValue(desc, "getBoolean", "ignoreChildSets", true);
    return result;
}
function indexOf(array, obj) {
    for (var k = 0; k < array.length; ++k) {
        if (array[k] === obj) {
            return k;
        }
    }
    return -1;
}
function getUserOptions() {
    // Note: When running from ExtendScript editor, editing the script soetimes destroys all previous options!
    var lastUserOptions = retrieveLastUserOptions();
    // If dialogs are turned off, we just return the last / default options
    // if ( app.playbackDisplayDialogs != DialogModes.ALL ) {
    //    return lastUserOptions;
    // }
    var sizeItems = ["Auto", 32, 64, 128, 256, 512, 1024, 2048, 4096];
    var sourceItems = ["Frame Animation", "Document Root Layers", "Specific Layer Set"];
    var layerSetItems = [];
    for (var k = 0; k < app.activeDocument.layerSets.length; ++k) {
        var layerSet = app.activeDocument.layerSets[k];
        layerSetItems[layerSetItems.length] = layerSet.name;
    }
    if (layerSetItems.length === 0) {
        layerSetItems[0] = "No Layer Sets Available";
    }
    var dlg = new Window("dialog", kScriptTitle);
    dlg.atlasSizeTitleGrp = dlg.add("group", undefined);
    dlg.atlasSizeTitleGrp.alignment = "left";
    dlg.atlasSizeTitleGrp.label01 = dlg.atlasSizeTitleGrp.add("StaticText", undefined, "Atlas Size (Pixels)");
    dlg.atlasSizeGrp = dlg.add("group", undefined);
    dlg.atlasSizeGrp.orientation = "row";
    dlg.atlasSizeGrp.alignment = "left";
    dlg.atlasSizeGrp.dropDownWidth = dlg.atlasSizeGrp.add("DropDownList", undefined, sizeItems);
    dlg.atlasSizeGrp.dropDownWidth.text = "Width: ";
    dlg.atlasSizeGrp.dropDownWidth.selection = 0;
    dlg.atlasSizeGrp.dropDownHeight = dlg.atlasSizeGrp.add("DropDownList", undefined, sizeItems);
    dlg.atlasSizeGrp.dropDownHeight.text = "Height: ";
    dlg.atlasSizeGrp.dropDownHeight.selection = 0;
    dlg.sourceTitleGrp = dlg.add("group", undefined);
    dlg.sourceTitleGrp.alignment = "left";
    dlg.sourceTitleGrp.label01 = dlg.sourceTitleGrp.add("StaticText", undefined, "Sprite Source");
    dlg.sourceGrp = dlg.add("group", undefined);
    dlg.sourceGrp.orientation = "column";
    dlg.sourceGrp.alignment = "left";
    dlg.sourceGrp.dropDownSource = dlg.sourceGrp.add("DropDownList", undefined, sourceItems);
    dlg.sourceGrp.dropDownSource.text = "Source: ";
    dlg.sourceGrp.dropDownSource.selection = 0;
    dlg.sourceGrp.dropDownSource.onChange = function () {
        userOptionsDlgUpdateLayerSetChildControls(dlg);
    };
    dlg.sourceGrp.dropDownLayerSet = dlg.sourceGrp.add("DropDownList", undefined, layerSetItems);
    dlg.sourceGrp.dropDownLayerSet.text = "Layer Set: ";
    dlg.sourceGrp.dropDownLayerSet.selection = 0;
    dlg.sourceGrp.ignoreChildSetsCheck = dlg.sourceGrp.add("CheckBox", undefined, "Ignore Child Layer Sets");
    dlg.sourceGrp.ignoreChildSetsCheck.value = true;
    dlg.otherTitleGrp = dlg.add("group", undefined);
    dlg.otherTitleGrp.alignment = "left";
    dlg.otherTitleGrp.label01 = dlg.otherTitleGrp.add("StaticText", undefined, "Other Options");
    dlg.flattenGrp = dlg.add("group");
    dlg.flattenGrp.orientation = "row";
    dlg.flattenGrp.alignment = "left";
    dlg.flattenGrp.flattenCheck = dlg.flattenGrp.add("CheckBox", undefined, "Flatten Image?");
    dlg.buttonGrp = dlg.add("group");
    dlg.buttonGrp.okButton = dlg.buttonGrp.add("button", undefined, "OK");
    dlg.buttonGrp.cancelButton = dlg.buttonGrp.add("button", undefined, "Cancel");
    // Before presenting, update from user options (if applicable)
    if (lastUserOptions.atlasWidth && lastUserOptions.atlasWidth > 0) {
        dlg.atlasSizeGrp.dropDownWidth.selection = indexOf(sizeItems, lastUserOptions.atlasWidth);
    }
    if (lastUserOptions.atlasHeight && lastUserOptions.atlasHeight > 0) {
        dlg.atlasSizeGrp.dropDownHeight.selection = indexOf(sizeItems, lastUserOptions.atlasHeight);
    }
    if (lastUserOptions.frameSource && lastUserOptions.frameSource > 0) {
        dlg.sourceGrp.dropDownSource.selection = lastUserOptions.frameSource;
    }
    if (lastUserOptions.flattenAtlas !== undefined) {
        dlg.flattenGrp.flattenCheck.value = lastUserOptions.flattenAtlas;
    }
    if (lastUserOptions.ignoreChildSets !== undefined) {
        dlg.sourceGrp.ignoreChildSetsCheck.value = lastUserOptions.ignoreChildSets;
    }
    if ((lastUserOptions.documentName === app.activeDocument.name) && (lastUserOptions.layerSetIndex < app.activeDocument.layerSets.length)) {
        dlg.sourceGrp.dropDownLayerSet.selection = lastUserOptions.layerSetIndex;
    }
    userOptionsDlgUpdateLayerSetChildControls(dlg);
    dlg.center();
    var dlgResult = dlg.show(); // 1 = ok, 2 = cancel
    // Note: .selection as a string is the value in the drop down, as an int it's the item's index!
    var userOptions = {};
    userOptions.documentName = app.activeDocument.name;
    userOptions.atlasWidth = parseInt(sizeItems[parseInt(dlg.atlasSizeGrp.dropDownWidth.selection)]);
    userOptions.atlasHeight = parseInt(sizeItems[parseInt(dlg.atlasSizeGrp.dropDownHeight.selection)]);
    userOptions.frameSource = parseInt(dlg.sourceGrp.dropDownSource.selection);
    userOptions.flattenAtlas = dlg.flattenGrp.flattenCheck.value;
    userOptions.layerSetIndex = parseInt(dlg.sourceGrp.dropDownLayerSet.selection);
    userOptions.ignoreChildSets = dlg.sourceGrp.ignoreChildSetsCheck.value;
    if (isNaN(userOptions.atlasWidth)) {
        userOptions.atlasWidth = 0;
    }
    if (isNaN(userOptions.atlasHeight)) {
        userOptions.atlasHeight = 0;
    }
    storeLastUserOptions(userOptions);
    if (dlgResult === 2) {
        return false;
    }
    return userOptions;
}
function userOptionsDlgUpdateLayerSetChildControls(dlg) {
    if (dlg.sourceGrp.dropDownSource.selection === SheetFrameSource.LayerSet) {
        dlg.sourceGrp.dropDownLayerSet.visible = true;
    }
    else {
        dlg.sourceGrp.dropDownLayerSet.visible = false;
    }
    if (dlg.sourceGrp.dropDownSource.selection === SheetFrameSource.FrameAnimation) {
        dlg.sourceGrp.ignoreChildSetsCheck.visible = false;
    }
    else {
        dlg.sourceGrp.ignoreChildSetsCheck.visible = true;
    }
}
function createDestDocWithOptions(userOptions, srcDoc) {
    if (userOptions === false) {
        return false;
    }
    var sourceType = userOptions.frameSource;
    var layerSetIndex = userOptions.layerSetIndex;
    var destDoc = false;
    if (sourceType === SheetFrameSource.FrameAnimation) {
        // How many frames in the animation?
        var frameCount = getFrameCount();
        if (frameCount === 0) {
            throw new Error("No animation frames were found in this file. In 'Frame Animation' mode this script requires a Photo Frame animation to create a sprite sheet.");
        }
        destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
        copyAnimationFrames(userOptions, srcDoc, destDoc, frameCount);
    }
    else if (sourceType === SheetFrameSource.DocumentLayers) {
        destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
        copyLayersAsSprites(userOptions, srcDoc, destDoc, srcDoc.layers);
    }
    else if (sourceType === SheetFrameSource.LayerSet) {
        if (layerSetIndex >= srcDoc.layerSets.length) {
            throw new Error("The 'Specific Layer Set' option was selected, but there is no selected layer set in the open document.");
        }
        if (srcDoc.layerSets[layerSetIndex].layers.length === 0) {
            throw new Error("The selected layer set contains no layers.");
        }
        destDoc = app.documents.add(srcDoc.width, srcDoc.height, srcDoc.resolution, srcDoc.name + " Sheet");
        copyLayersAsSprites(userOptions, srcDoc, destDoc, srcDoc.layerSets[layerSetIndex].layers);
    }
    app.activeDocument = destDoc;
    return destDoc;
}
function copyAnimationFrames(userOptions, srcDoc, destDoc, frameCount) {
    // Copy Merged Frames across to new image
    for (var k = 1; k <= frameCount; k++) {
        app.activeDocument = srcDoc;
        goToFrame(k);
        // original code:
        // selectAllLayers();
        // duplicateSelectedLayers();
        // mergeSelectedLayers();
        srcDoc.mergeVisibleLayers();
        var layer = srcDoc.activeLayer.duplicate(destDoc, ElementPlacement.PLACEATEND);
        // go back the mergeVisibleLayers step
        var idRvrt = app.charIDToTypeID("Rvrt");
        app.executeAction(idRvrt, undefined, DialogModes.NO);
        deleteSelectedLayers();
        app.activeDocument = destDoc;
        layer.name = "Frame " + k;
    }
}
function copyLayersAsSprites(userOptions, srcDoc, destDoc, layers) {
    app.activeDocument = srcDoc;
    if (srcDoc === undefined || destDoc === undefined || layers === undefined) {
        throw new Error("copyLayersAsSprites called with undefined parameter(s).");
    }
    var wasVisible = [];
    var bgLayerIndex = -1;
    var wasParentVisible;
    // Pre process
    for (var k = 0; k < layers.length; k++) {
        wasVisible[k] = layers[k].visible;
        if (layers[k].isBackgroundLayer) {
            bgLayerIndex = k;
            layers[k].isBackgroundLayer = false;
            layers[k].visible = wasVisible[k]; // Toggling background layer affects visibility
        }
    }
    if (layers[0].parent.visible !== undefined) {
        wasParentVisible = layers[0].parent.visible;
        layers[0].parent.visible = true;
    }
    // Do the work
    for (var k = 0; k < layers.length; k++) {
        app.activeDocument = srcDoc;
        // Don't process background layer as it causes issues (probably don't want it as a sprite anyway)
        if (k === bgLayerIndex) {
            continue;
        }
        // If we are ignoring child sets, check and skip if necessary
        if (userOptions.ignoreChildSets && layers[k].layers !== undefined && layers[k].layers.length > 0) {
            continue;
        }
        // Turn off all layers (but don't mess with the background layer)
        for (var j = 0; j < layers.length; j++) {
            if (j !== bgLayerIndex) {
                layers[j].visible = false;
            }
        }
        // Turn on active layer
        layers[k].visible = true;
        // Turn into sprite!
        selectAllLayers();
        duplicateSelectedLayers();
        mergeSelectedLayers();
        var layer = srcDoc.activeLayer.duplicate(destDoc, ElementPlacement.PLACEATEND);
        deleteSelectedLayers();
        app.activeDocument = destDoc;
        layer.name = "Sprite " + (k + 1);
    }
    // Undo Pre-Process
    app.activeDocument = srcDoc;
    for (var k = 0; k < layers.length; k++) {
        if (k === bgLayerIndex) {
            layers[k].isBackgroundLayer = true;
        }
    }
    for (var k = 0; k < layers.length; k++) {
        layers[k].visible = wasVisible[k];
    }
    if (layers[0].parent.visible !== undefined) {
        layers[0].parent.visible = wasParentVisible;
    }
}
function atlasSizeForFrames(userOptions, frameWidth, frameHeight, frameCount) {
    var atlasWidth = 16;
    var atlasHeight = 16;
    var didFit = false;
    var fixedWidth = false;
    var fixedHeight = false;
    if (userOptions.atlasWidth > 0) {
        atlasWidth = userOptions.atlasWidth;
        fixedWidth = true;
    }
    if (userOptions.atlasHeight > 0) {
        atlasHeight = userOptions.atlasHeight;
        fixedHeight = true;
    }
    if (fixedWidth && fixedHeight) {
        return { width: atlasWidth, height: atlasHeight };
    }
    while (didFit === false) {
        // $.writeln("Trying to fit " + frameCount + "x" + frameWidth + "x" + frameHeight + " onto: " + atlasWidth + " x " + atlasHeight);
        var gridWidth = Math.floor(atlasWidth / frameWidth);
        var requiredGridHeight = Math.ceil(frameCount / gridWidth);
        var requiredPixelHeight = requiredGridHeight * frameHeight;
        if (requiredPixelHeight > atlasHeight) {
            if ((atlasWidth <= atlasHeight) && (fixedWidth === false)) {
                atlasWidth *= 2;
            }
            else {
                atlasHeight *= 2;
            }
            didFit = false;
        }
        else {
            didFit = true;
        }
    }
    var result = { width: atlasWidth, height: atlasHeight };
    // $.writeln("Atlas Size: " + result.width + " x " + result.height);
    return result;
}
function layoutLayersAsSprites(userOptions, destDoc) {
    app.activeDocument = destDoc;
    // Figure out atlas size
    var frameWidth = parseInt(destDoc.width);
    var frameHeight = parseInt(destDoc.height);
    var frameCount = destDoc.layers.length;
    var atlasSize = atlasSizeForFrames(userOptions, frameWidth, frameHeight, frameCount);
    destDoc.resizeCanvas(atlasSize.width, atlasSize.height, AnchorPosition.TOPLEFT);
    // Layout Layers
    var x = 0;
    var y = 0;
    for (var k = 0; k < frameCount; ++k) {
        try {
            destDoc.layers[k].translate(x, y);
        }
        catch (e) { /* ignore error */ }
        x += frameWidth;
        if ((x + frameWidth) > atlasSize.width) {
            x = 0;
            y += frameHeight;
        }
    }
    // Layout sprites
    for (var k = 0; k < frameCount; k++) {
        // tslint:disable-next-line:no-unused-expression
        destDoc.layers[k];
    }
    if (userOptions.flattenAtlas === true) {
        selectAllLayers();
        mergeSelectedLayers();
    }
}
//
// Animation Actions
//
// Count the number of frames in the current frame animation (brute force, slow).
function getFrameCount() {
    for (var k = 1; k < 999; k++) {
        if (goToFrame(k) === false) {
            return k - 1;
        }
    }
    return 0;
}
// Jump to the given frame in the frame animation in the active document
function goToFrame(jumpToFrame) {
    try {
        var desc = new ActionDescriptor();
        var ref1 = new ActionReference();
        ref1.putIndex(app.stringIDToTypeID("animationFrameClass"), jumpToFrame);
        desc.putReference(app.charIDToTypeID("null"), ref1);
        app.executeAction(app.charIDToTypeID("slct"), desc, DialogModes.NO);
        return true;
    }
    catch (e) {
        // $.writeln(e);
    }
    return false;
}
//
// Layer Editing Actions
//
// Select all layers in the active document
function selectAllLayers() {
    var idselectAllLayers = app.stringIDToTypeID("selectAllLayers");
    var desc4 = new ActionDescriptor();
    var idnull = app.charIDToTypeID("null");
    var ref1 = new ActionReference();
    var idLyr = app.charIDToTypeID("Lyr ");
    var idOrdn = app.charIDToTypeID("Ordn");
    var idTrgt = app.charIDToTypeID("Trgt");
    ref1.putEnumerated(idLyr, idOrdn, idTrgt);
    desc4.putReference(idnull, ref1);
    app.executeAction(idselectAllLayers, desc4, DialogModes.NO);
}
// Duplicate all layers that are currently selected
function duplicateSelectedLayers() {
    var idDplc = app.charIDToTypeID("Dplc");
    var desc5 = new ActionDescriptor();
    var idnull = app.charIDToTypeID("null");
    var ref2 = new ActionReference();
    var idLyr = app.charIDToTypeID("Lyr ");
    var idOrdn = app.charIDToTypeID("Ordn");
    var idTrgt = app.charIDToTypeID("Trgt");
    ref2.putEnumerated(idLyr, idOrdn, idTrgt);
    desc5.putReference(idnull, ref2);
    var idVrsn = app.charIDToTypeID("Vrsn");
    desc5.putInteger(idVrsn, 2);
    app.executeAction(idDplc, desc5, DialogModes.NO);
}
// Merge all currently selected layers
function mergeSelectedLayers() {
    var idMrgtwo = app.charIDToTypeID("Mrg2");
    try {
        app.executeAction(idMrgtwo, undefined, DialogModes.NO);
    }
    catch (e) { /* ignore error */ }
}
// Delete all selected layers
function deleteSelectedLayers() {
    // Delete selected layers
    var idDlt = app.charIDToTypeID("Dlt ");
    var desc8 = new ActionDescriptor();
    var idnull = app.charIDToTypeID("null");
    var ref6 = new ActionReference();
    var idLyr = app.charIDToTypeID("Lyr ");
    var idOrdn = app.charIDToTypeID("Ordn");
    var idTrgt = app.charIDToTypeID("Trgt");
    ref6.putEnumerated(idLyr, idOrdn, idTrgt);
    desc8.putReference(idnull, ref6);
    try {
        app.executeAction(idDlt, desc8, DialogModes.NO);
    }
    catch (e) { /* ignore error */ }
}

$.writeln("init exportSprite before");
function savePNGFile(document, path) {
    // set save options
    var opts = new ExportOptionsSaveForWeb();
    opts.PNG8 = false;
    opts.transparency = true;
    opts.interlaced = false;
    opts.quality = 100;
    opts.includeProfile = false;
    opts.format = SaveDocumentType.PNG; // Document Type
    // save png file in same folder as open doc
    document.exportDocument(new File(path), ExportType.SAVEFORWEB, opts);
}
function exportSprite(options) {
    var olddoc = app.activeDocument;
    // alert(options.documentName);
    var newdoc = createSpriteSheetInOtherDocument(options);
    var docPath = olddoc.path;
    var newPath = docPath + "/" + options.fileName;
    savePNGFile(newdoc, newPath);
    // alert("after-gen");
    newdoc.close(SaveOptions.DONOTSAVECHANGES);
    // alert("after close");
}
_global.exportSprite = exportSprite;
$.writeln("after exportSprite before");

/// <reference path="../types/index.d.ts" />
function doTilingFlip(horizontal, vertical) {
    //alert(horizontal + " .. " + vertical)
    var meta = loadFileMetaData();
    if (horizontal)
        meta.flippedHorizontal = !meta.flippedHorizontal;
    if (vertical)
        meta.flippedVertical = !meta.flippedVertical;
    saveFileMetaData(meta);
    var doc = app.activeDocument;
    var layers = doc.artLayers;
    var length = doc.artLayers.length;
    var height = doc.height;
    var width = doc.width;
    var _percent50 = new UnitValue("50%");
    var _percent0 = new UnitValue("0%");
    for (var i = 0; i < length; i++) {
        var layer = doc.artLayers[i];
        if (layer.name.indexOf(":f") !== -1 && layer.name.indexOf("metadata: ") === -1) {
            //layer.applyOffset(Math.floor(width / 2), Math.floor(height / 2), OffsetUndefinedAreas.WRAPAROUND);
            layer.applyOffset(horizontal ? _percent50 : _percent0, vertical ? _percent50 : _percent0, OffsetUndefinedAreas.WRAPAROUND);
        }
    }
}
_global.doTilingFlip = doTilingFlip;

$.write("before main");
$.write(_global);
